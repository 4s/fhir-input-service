<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

  <groupId>dk.s4.microservices</groupId>
	<artifactId>fhir-input-service</artifactId>
	<version>13.0.26</version>
	<packaging>war</packaging>

  <name>FHIR Input Service</name>

    <properties>
        <microservice-common-version>7.3.2</microservice-common-version>
        <messaging-version>14.2.4</messaging-version>
        <hapifhir-version>3.7.0</hapifhir-version>
        <jetty_version>9.4.12.v20180830</jetty_version>
        <spring_version>5.1.3.RELEASE</spring_version>
        <webResourceDir>src/main/webapp/WEB-INF/without-keycloak</webResourceDir>
        <sample-data>4.1.0</sample-data>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    </properties>

	<repositories>
        <repository>
            <id>alexandranexus</id>
            <name>Alexandra RELEASES</name>
            <url>https://maven.alexandra.dk/repository/maven-releases</url>
        </repository>
	</repositories>

    <dependencies>
        <dependency>
            <groupId>com.github.tomakehurst</groupId>
            <artifactId>wiremock-standalone</artifactId>
            <version>2.27.2</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>dk.s4.microservices</groupId>
            <artifactId>messaging</artifactId>
            <version>${messaging-version}</version>
        </dependency>

        <dependency>
            <groupId>dk.s4.microservices</groupId>
            <artifactId>microservice-common</artifactId>
            <version>${microservice-common-version}</version>
        </dependency>

        <dependency>
            <groupId>dk.s4.sampledata</groupId>
            <artifactId>sample-data</artifactId>
            <version>${sample-data}</version>
            <scope>test</scope>
        </dependency>

        <!-- This dependency includes the core HAPI-FHIR classes -->
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-base</artifactId>
            <version>${hapifhir-version}</version>
        </dependency>
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-structures-r4</artifactId>
            <version>${hapifhir-version}</version>
        </dependency>
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-jpaserver-base</artifactId>
            <version>${hapifhir-version}</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-io</artifactId>
            <version>${jetty_version}</version>
        </dependency>

        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-server</artifactId>
            <version>${jetty_version}</version>
        </dependency>

        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-webapp</artifactId>
            <version>${jetty_version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring_version}</version>
            <!--<scope>provided</scope>-->
        </dependency>

        <!-- Contains KeycloakDeployment and KeycloakConfigResolver -->
        <dependency>
            <groupId>org.keycloak</groupId>
            <artifactId>keycloak-adapter-core</artifactId>
            <scope>provided</scope>
            <version>3.1.0.Final</version>
        </dependency>

        <dependency>
            <groupId>org.keycloak</groupId>
            <artifactId>keycloak-adapter-spi</artifactId>
            <scope>provided</scope>
            <version>3.1.0.Final</version>
        </dependency>

        <!-- Contains KeycloakPrincipal -->
        <dependency>
            <groupId>org.keycloak</groupId>
            <artifactId>keycloak-core</artifactId>
            <scope>provided</scope>
            <version>3.1.0.Final</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.9.5</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.googlecode.json-simple</groupId>
            <artifactId>json-simple</artifactId>
            <version>1.1</version>
        </dependency>

        <dependency>
            <groupId>com.github.stefanbirkner</groupId>
            <artifactId>system-rules</artifactId>
            <version>1.17.0</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.2</version>
        </dependency>

        <dependency>
            <groupId>net.logstash.logback</groupId>
            <artifactId>logstash-logback-encoder</artifactId>
            <version>4.11</version>
        </dependency>

        <!--Prometheus-->
        <dependency>
            <groupId>io.prometheus</groupId>
            <artifactId>simpleclient</artifactId>
            <version>0.6.0</version>
        </dependency>

        <!--HotspotJVMmetrics-->
        <dependency>
            <groupId>io.prometheus</groupId>
            <artifactId>simpleclient_hotspot</artifactId>
            <version>0.6.0</version>
        </dependency>

        <!--Expositionservlet-->
        <dependency>
            <groupId>io.prometheus</groupId>
            <artifactId>simpleclient_servlet</artifactId>
            <version>0.0.26</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.assertj/assertj-core -->
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>3.12.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>withKeycloak</id>
            <properties>
                <!-- Default folder with web.xml and jetty-web.xml *with* Keycloak integration -->
                <webResourceDir>src/main/webapp/WEB-INF/with-keycloak</webResourceDir>
            </properties>
        </profile>
    </profiles>

    <build>
    <!-- Tells Maven to name the generated WAR file as service.war -->
    <finalName>service</finalName>
     <pluginManagement>
        <plugins>
            <plugin>
                <!-- Makes unittest execute sequentially -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M3</version>
                <configuration>
                    <forkCount>1</forkCount>
                    <reuseForks>false</reuseForks>
                </configuration>
            </plugin>
            <!-- Tell Maven which Java source version you want to use and what compiler to use -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.0</version>
                <configuration>
                    <release>8</release>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <source>8</source>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>2.12.1</version>
                <executions>
                    <execution>
                        <configuration>
                            <configLocation>https://bitbucket.org/4s/coding-style-guides/raw/master/checkstyle.xml</configLocation>
                            <consoleOutput>true</consoleOutput>
                            <failsOnError>true</failsOnError>
                        </configuration>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>copy-web-resource-files</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target name="copy resource files to WEB-INF">
                                <copy todir="src/main/webapp/WEB-INF" overwrite="true">
                                    <fileset dir="${webResourceDir}">
                                        <include name="**/*"/>
                                    </fileset>
                                </copy>
                            </target>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <configuration>
                    <webResources>
                        <resource>
                            <directory>${webResourceDir}</directory>
                        </resource>
                    </webResources>
                    <archive>
                        <manifestEntries>
                            <Build-Time>${maven.build.timestamp}</Build-Time>
                        </manifestEntries>
                    </archive>
                    <webXml>${webResourceDir}/web.xml</webXml>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>${jetty_version}</version>
                <configuration>
                    <webXml>${webResourceDir}/web.xml</webXml>
                    <jettyXml>${webResourceDir}/jetty-web.xml</jettyXml>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.7.1</version>
            </plugin>
        </plugins>
        </pluginManagement>
    </build>
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.0.1</version>
                <configuration>
                </configuration>
            </plugin>
        </plugins>
    </reporting>
    <distributionManagement>
        <site>
            <id>4s-online.dk</id>
            <url>ftp://ftp.4s-online.dk/</url>
        </site>
    </distributionManagement>
</project>

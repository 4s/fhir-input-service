package dk.s4.microservices.fhirinputservice.servlet;

import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.fhirinputservice.test.FhirInputServiceTest;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.CarePlanVerifier;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ResourceConsolidatorTest {
    private static final Logger logger = LoggerFactory.getLogger(ResourceConsolidatorTest.class);
    private static String ENVIRONMENT_FILE = "service.env";
    private static String DEPLOY_FILE = "deploy.env";
    private static IParser jsonParser;

    private static ResourceConsolidator resourceConsolidator;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void consolidateObservation() {
    }

    @Test
    public void consolidateQuestionnaireResponse() {
    }

    @Test
    public void consolidateBundleAndGetCodingQMLinksBundle() throws IOException {
        MDC.put("correlationId", MessagingUtils.verifyOrCreateId(null));
        MDC.put("transactionId", UUID.randomUUID().toString());

        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Q-M-links/Bundle.json");
        Resource resource = (Resource) jsonParser.parseResource(resourceString);

        List<String> codes = resourceConsolidator.consolidateBundleAndGetCoding((Bundle) resource);
        Assert.assertEquals(3, codes.size());
        Assert.assertTrue(codes.contains("MDC188736"));
        Assert.assertTrue(codes.contains("MDC150020"));
        Assert.assertTrue(codes.contains("MDC149546"));
    }

    @Test
    public void consolidateBundleAndGetCodingOfflineCTGBundle() throws IOException {
        MDC.put("correlationId", MessagingUtils.verifyOrCreateId(null));
        MDC.put("transactionId", UUID.randomUUID().toString());

        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/CTG/offline_ex_001.json");
        Resource resource = (Resource) jsonParser.parseResource(resourceString);

        List<String> codes = resourceConsolidator.consolidateBundleAndGetCoding((Bundle) resource);
        Assert.assertEquals(1, codes.size());
        Assert.assertTrue(codes.contains("MDC8450059"));
    }

    @Test
    public void consolidateBundleAndGetCodingFirstOnlineCTGBundle() throws IOException {
        MDC.put("correlationId", MessagingUtils.verifyOrCreateId(null));
        MDC.put("transactionId", UUID.randomUUID().toString());

        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/CTG/online_ex_001/1.json");
        Resource resource = (Resource) jsonParser.parseResource(resourceString);

        List<String> codes = resourceConsolidator.consolidateBundleAndGetCoding((Bundle) resource);
        Assert.assertEquals(1, codes.size());
        Assert.assertTrue(codes.contains("MDC8450059"));
    }

    @Test
    public void consolidateBundleAndGetCodingWoundRegDetailWithImage() throws IOException {
        MDC.put("correlationId", MessagingUtils.verifyOrCreateId(null));
        MDC.put("transactionId", UUID.randomUUID().toString());

        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Wounds/Bundle_wound_registration_registrationDetail_image.json");
        Resource resource = (Resource) jsonParser.parseResource(resourceString);

        List<String> codes = resourceConsolidator.consolidateBundleAndGetCoding((Bundle) resource);
        Assert.assertEquals(6, codes.size());
        Assert.assertTrue(codes.contains("SNOMED401191002"));
        Assert.assertTrue(codes.contains("SNOMED405161002"));
        Assert.assertTrue(codes.contains("SNOMED298007001"));
        Assert.assertTrue(codes.contains("HD360wound-inflammation-code"));
        Assert.assertTrue(codes.contains("HD360activity-level-code"));
        Assert.assertTrue(codes.contains("HD360wound-size-code"));
    }

    @BeforeClass
    public static void beforeClass() throws Exception {

        jsonParser = FhirInputService.getMyFhirContext().newJsonParser();
        jsonParser.setPrettyPrint(true);
        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = FhirInputServiceTest.class.getClassLoader().getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        logger.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        try {
            Properties properties = new Properties();
            FileInputStream environmentFile = new FileInputStream(path + "/" + ENVIRONMENT_FILE);
            properties.load(environmentFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
            FileInputStream deployFile = new FileInputStream(path + "/" + DEPLOY_FILE);
            properties.load(deployFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
        }
        catch (IOException | IllegalArgumentException e) {
            logger.error("Failed loading file from: {}. Error message: {}",  path + "/" + ENVIRONMENT_FILE,
                    e.getMessage());
        }

        //Mocks to avoid calling external services during test.
        PatientResolver patientResolver = mock(PatientResolver.class);
        DeviceResolver deviceResolver = mock(DeviceResolver.class);
        MediaFacade mediaFacade = mock(MediaFacade.class);
        CarePlanVerifier carePlanVerifier = mock(CarePlanVerifier.class);
        doReturn(true).when(carePlanVerifier).isActiveCarePlan(anyObject());
        resourceConsolidator = new ResourceConsolidator(patientResolver, deviceResolver, mediaFacade, carePlanVerifier);

        Identifier ptIdentifier = new Identifier()
                .setSystem(System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM"))
                .setValue("2512489996");
        Reference patientRef = new Reference().setIdentifier(ptIdentifier).setType("Patient");
        Identifier devIdentifier = new Identifier()
                .setSystem(System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"))
                .setValue("MDC1234");
        Reference deviceRef = new Reference().setIdentifier(devIdentifier).setType("Device");

        when(patientResolver.getReference(any())).thenReturn(patientRef);
        when(deviceResolver.getReference(any())).thenReturn(deviceRef);


        FhirInputService.eventProducer = new MockKafkaEventProducer("ResourceConsolidatorTest");
    }
}
package dk.s4.microservices.fhirinputservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.messaging.EventProducer;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.security.ThreadLocalContext;
import org.hl7.fhir.r4.model.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.MDC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

public class ResourceHandlerTest {

    private Topic topic;
    private Message message;
    private EventProducer producer;
    private FhirContext context;

    @Before
    public void setup() throws IOException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        producer = mock(EventProducer.class);
        IParser iParser = mock(IParser.class);

        doAnswer(invocation -> {
            topic = (Topic) invocation.getArguments()[0];
            message = (Message) invocation.getArguments()[1];
            return null;
        }).when(producer).sendMessage(any(Topic.class), any(Message.class));

        when(context.newJsonParser()).thenReturn(iParser);
        when(iParser.encodeResourceToString(anyObject())).thenReturn("theBody");
    }

    @Test
    public void sendResourceToMsgQueue() throws InterruptedException {
        Task task = new Task();
        List<String> codes = new ArrayList<>();
        String codeString = "codeString";
        codes.add(codeString);
        MDC.put(System.getenv("CORRELATION_ID"),"theCorrelationId");
        ThreadLocalContext.put(System.getenv("TRANSACTION_ID"), "theTransactionId");
        ThreadLocalContext.put("SECURITY_TOKEN", "token");

        ResourceHandler.sendResourceToMsgQueue(task, "Task", codes, "SESSION", producer, context);

        //Assert Topic is correct
        assertEquals(topic.getOperation(), Topic.Operation.InputReceived);
        assertEquals(topic.getDataCategory(), Topic.Category.FHIR);
        assertEquals(topic.getDataType(), "Task");
        assertEquals(topic.getDataCodes().size(), 1);
        assertEquals(topic.getDataCodes().get(0), codeString);

        //Assert Message is correct
        assertEquals(message.getSender(), System.getenv("SERVICE_NAME"));
        assertEquals(message.getBodyCategory(), BodyCategory.FHIR);
        assertEquals(message.getBodyType(), "Task");
        assertEquals(message.getContentVersion(), System.getenv("FHIR_VERSION"));
        assertEquals(message.getCorrelationId(), "theCorrelationId");
        assertEquals(message.getTransactionId(), "theTransactionId");
        assertEquals(message.getSession(), "SESSION");
        assertEquals(message.getSecurity(), "token");
        assertEquals(message.getBody(), "theBody");
    }
}
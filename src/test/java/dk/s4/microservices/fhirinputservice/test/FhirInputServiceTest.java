package dk.s4.microservices.fhirinputservice.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.PreferReturnEnum;
import ca.uhn.fhir.rest.client.api.IClientInterceptor;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.IHttpRequest;
import ca.uhn.fhir.rest.client.api.IHttpResponse;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import dk.s4.microservices.fhirinputservice.provider.ResourceHandler;
import dk.s4.microservices.fhirinputservice.servlet.DeviceResolver;
import dk.s4.microservices.fhirinputservice.servlet.FhirInputService;
import dk.s4.microservices.fhirinputservice.servlet.MediaFacade;
import dk.s4.microservices.fhirinputservice.servlet.PatientResolver;
import dk.s4.microservices.fhirinputservice.servlet.ResourceConsolidator;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.MessageParseException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.CarePlanVerifier;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Media;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FhirInputServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(FhirInputServiceTest.class);

	private static IGenericClient ourClient;
	private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(FhirInputServiceTest.class);

	private static int ourPort;

	private static Server ourServer;
	private static String ourServerBase;

    private static IParser jsonParser;

    private static String ENVIRONMENT_FILE = "service.env";
    private static String DEPLOYMENT_FILE = "deploy.env";

    private PatientResolver patientResolver;
    private DeviceResolver deviceResolver;
    private MediaFacade mediaFacade;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testQMLinksBundleCreate() throws IOException, InterruptedException, ExecutionException, MessageParseException, TimeoutException, ParseException {
        ourLog.info("testQMLinksBundleCreate");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("MDC"+"188736")
                .addDataCode("MDC"+"150020")
                .addDataCode("MDC"+"149546");

        Resource outputResource = createResource("FHIR-clean/Q-M-links/Bundle.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(4, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("QuestionnaireResponse"));
        assertTrue(outputBundle.getEntry().get(1).getResource().fhirType().equals("Observation"));
        assertTrue(outputBundle.getEntry().get(2).getResource().fhirType().equals("Observation"));
        assertTrue(outputBundle.getEntry().get(3).getResource().fhirType().equals("Observation"));

        QuestionnaireResponse qr = (QuestionnaireResponse)outputBundle.getEntry().get(0).getResource();
        testPatientRef(qr.getSubject());
        testPatientRef(qr.getAuthor());
        testPatientRef(qr.getSource());

        Observation obs1 = (Observation) outputBundle.getEntry().get(1).getResource();
        testPatientRef(obs1.getSubject());
        testPatientRef(obs1.getPerformer().get(0));

        Observation obs2 = (Observation) outputBundle.getEntry().get(2).getResource();
        testPatientRef(obs2.getSubject());
        testPatientRef(obs2.getPerformer().get(0));

        Observation obs3 = (Observation) outputBundle.getEntry().get(3).getResource();
        testPatientRef(obs3.getSubject());
        testPatientRef(obs3.getPerformer().get(0));

        deviceCaptor = ArgumentCaptor.forClass(Device.class);
        referenceCaptor = ArgumentCaptor.forClass(Reference.class);
        List<Device> devices = deviceCaptor.getAllValues();
        verify(deviceResolver, times(6)).createConditionally(deviceCaptor.capture(), referenceCaptor.capture());

        //List<Device> devices = deviceCaptor.getAllValues();
        for (Device device: devices) {
            Assert.assertTrue(device.getId().equals("00091FFEFF801A33.00091F801A33")
                    || device.getId().equals("7EEDABEE34ADBEEF.C4B301D280C6"));
        }
        List<Reference> references = referenceCaptor.getAllValues();
        for (Reference reference: references) {
            Assert.assertEquals(System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"), reference.getIdentifier().getSystem());
            Assert.assertEquals("MDC1234", reference.getIdentifier().getValue());
        }
    }

    @Test
    public void testCTGofflineBundleCreate() throws InterruptedException, ParseException, IOException,
            ExecutionException, TimeoutException, MessageParseException {

        ourLog.info("testCTGofflineBundleCreate");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("MDC"+"8450059");

        Resource outputResource = createResource("FHIR-clean/CTG/offline_ex_001.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(1, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("Observation"));

        Observation obs = (Observation) outputBundle.getEntry().get(0).getResource();
        testPatientRef(obs.getSubject());
        testPatientRef(obs.getPerformer().get(0));

        deviceCaptor = ArgumentCaptor.forClass(Device.class);
        referenceCaptor = ArgumentCaptor.forClass(Reference.class);
        List<Device> devices = deviceCaptor.getAllValues();
        verify(deviceResolver, atLeast(1)).createConditionally(deviceCaptor.capture(), referenceCaptor.capture());

        //List<Device> devices = deviceCaptor.getAllValues();
        for (Device device: devices) {
            Assert.assertTrue(device.getId().equals("008098FEFF0E3913.0080980E3913")
                    || device.getId().equals("7EEDABEE34ADBEEF.C4B301D280C6"));
        }
        List<Reference> references = referenceCaptor.getAllValues();
        for (Reference reference: references) {
            Assert.assertEquals(System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"), reference.getIdentifier().getSystem());
            Assert.assertEquals("MDC1234", reference.getIdentifier().getValue());
        }
    }

    @Captor
    ArgumentCaptor<Device> deviceCaptor;

    @Captor
    ArgumentCaptor<Reference> referenceCaptor;

    @Captor
    ArgumentCaptor<Media> mediaCaptor;

    @Test
    public void testCTGofflineBundleDeviceCreateIssue() throws Exception {
        ourLog.info("testCTGofflineBundleDeviceCreateIssue");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("MDC"+"8450059");

        Resource outputResource = createResource("bundleDeviceCreateIssue.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(1, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("Observation"));

        Observation obs = (Observation) outputBundle.getEntry().get(0).getResource();
        testPatientRef(obs.getSubject());
        testPatientRef(obs.getPerformer().get(0));

        deviceCaptor = ArgumentCaptor.forClass(Device.class);
        referenceCaptor = ArgumentCaptor.forClass(Reference.class);
        verify(deviceResolver, times(6)).createConditionally(deviceCaptor.capture(), referenceCaptor.capture());

        List<Device> devices = deviceCaptor.getAllValues();
        for (Device device: devices) {
            Assert.assertEquals("008098FEFF0E3913.0080980E3913", device.getId());
        }
        List<Reference> references = referenceCaptor.getAllValues();
        for (Reference reference: references) {
            Assert.assertEquals(System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"), reference.getIdentifier().getSystem());
            Assert.assertEquals("MDC1234", reference.getIdentifier().getValue());
        }
    }

    @Test
    public void testCTGonlineFirstBundle() throws InterruptedException, ParseException, IOException, ExecutionException,
            TimeoutException, MessageParseException {
        ourLog.info("testCTGonlineFirstBundle");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("MDC"+"8450059");

        Resource outputResource = createResource("FHIR-clean/CTG/online_ex_001/1.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(1, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("Observation"));

        Observation obs = (Observation) outputBundle.getEntry().get(0).getResource();
        testPatientRef(obs.getSubject());
        testPatientRef(obs.getPerformer().get(0));

        deviceCaptor = ArgumentCaptor.forClass(Device.class);
        referenceCaptor = ArgumentCaptor.forClass(Reference.class);
        verify(deviceResolver, times(7)).createConditionally(deviceCaptor.capture(), referenceCaptor.capture());

        List<Device> devices = deviceCaptor.getAllValues();
        for (Device device: devices) {
            Assert.assertTrue(device.getId().equals("008098FEFF0E3913.0080980E3913")
                    || device.getId().equals("7EEDABEE34ADBEEF.C4B301D280C6"));
        }
        List<Reference> references = referenceCaptor.getAllValues();
        for (Reference reference: references) {
            Assert.assertEquals(System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"), reference.getIdentifier().getSystem());
            Assert.assertEquals("MDC1234", reference.getIdentifier().getValue());
        }
    }

    @Test
    public void testCTGonlineObservation() throws InterruptedException, ParseException, IOException, ExecutionException,
            TimeoutException, MessageParseException {
        ourLog.info("testCTGonlineObservation");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Observation")
                .addDataCode("MDC"+"8450059");

        Resource outputResource = createResource("FHIR-clean/CTG/online_ex_001/2.json", outputTopic);

        ourLog.trace("Bundle after consolidation: \n" +
                FhirInputService.getMyFhirContext().newJsonParser().setPrettyPrint(true).encodeResourceToString(outputResource));

        assertTrue(outputResource.fhirType().equals("Observation"));
    }

    @Test
    public void testCreateWound() throws InterruptedException, ParseException, IOException, ExecutionException, TimeoutException, MessageParseException {
        ourLog.info("testCreateWound");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED"+"416462003");

        Resource outputResource = createResource("FHIR-clean/Wounds/Bundle_create_wound.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(1, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("Observation"));

        Observation obs = (Observation) outputBundle.getEntry().get(0).getResource();
        testPatientRef(obs.getSubject());
        testPatientRef(obs.getPerformer().get(0));
    }

    @Test
    public void testUpdateWound() throws InterruptedException, ParseException, IOException, ExecutionException, TimeoutException, MessageParseException {
        ourLog.info("testUpdateWound");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED"+"416462003");

        Resource outputResource = createResource("FHIR-clean/Wounds/Bundle_update_wound.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(1, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("Observation"));

        Observation obs = (Observation) outputBundle.getEntry().get(0).getResource();
        testPatientRef(obs.getSubject());
        testPatientRef(obs.getPerformer().get(0));
    }

    @Test
    public void testWoundRegistrationDetail() throws InterruptedException, ParseException, IOException, ExecutionException, TimeoutException, MessageParseException {
        ourLog.info("testWoundRegistrationDetail");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED401191002")
                .addDataCode("SNOMED405161002")
                .addDataCode("SNOMED298007001")
                .addDataCode("HD360wound-inflammation-code")
                .addDataCode("HD360activity-level-code")
                .addDataCode("HD360wound-size-code");

        Resource outputResource = createResource("FHIR-clean/Wounds/Bundle_wound_registration_registrationDetail.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(6, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("Observation"));

        Observation obs = (Observation) outputBundle.getEntry().get(0).getResource();
        testPatientRef(obs.getSubject());
        testPatientRef(obs.getPerformer().get(0));
    }

    @Test
    public void testWoundRegistrationDetailWithImage() throws InterruptedException, ParseException, IOException, ExecutionException, TimeoutException, MessageParseException {
        ourLog.info("testWoundRegistrationDetail");

        setupMocks();

        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Bundle")
                .addDataCode("SNOMED401191002")
                .addDataCode("SNOMED405161002")
                .addDataCode("SNOMED298007001")
                .addDataCode("HD360wound-inflammation-code")
                .addDataCode("HD360activity-level-code")
                .addDataCode("HD360wound-size-code");

        Resource outputResource = createResource("FHIR-clean/Wounds/Bundle_wound_registration_registrationDetail_image.json", outputTopic);

        assertTrue(outputResource.fhirType().equals("Bundle"));
        Bundle outputBundle = (Bundle) outputResource;
        assertEquals(6, outputBundle.getEntry().size());
        assertTrue(outputBundle.getEntry().get(0).getResource().fhirType().equals("Observation"));

        Observation obs = (Observation) outputBundle.getEntry().get(0).getResource();
        testPatientRef(obs.getSubject());
        testPatientRef(obs.getPerformer().get(0));

        mediaCaptor = ArgumentCaptor.forClass(Media.class);
        referenceCaptor = ArgumentCaptor.forClass(Reference.class);
        verify(mediaFacade, times(1)).create(mediaCaptor.capture(), referenceCaptor.capture());

        List<Media> mediaList = mediaCaptor.getAllValues();
        for (Media media: mediaList) {
            Assert.assertTrue(media.getIdentifierFirstRep().getValue().equals("urn:uuid:e9f0c501-0e02-4933-ab0a-9fd1f2b87b06"));
            //Check consolidation:
            Assert.assertTrue(media.getPartOfFirstRep().getIdentifier().getValue().equals("urn:uuid:0c8b5272-0353-4b99-a15d-0714747b6b17"));
            Assert.assertTrue(media.getSubject().getIdentifier().getValue().equals("urn:uuid:c3a7ca02-cf8f-4fb1-a381-d446165b526f"));
            Assert.assertTrue(media.getOperator().getIdentifier().getValue().equals("urn:uuid:c3a7ca02-cf8f-4fb1-a381-d446165b526f"));
            Assert.assertTrue(media.getDevice().getIdentifier().getValue().equals("MDC1234"));
        }
    }

    private void setupMocks() {
        patientResolver = mock(PatientResolver.class);
        deviceResolver = mock(DeviceResolver.class);
        mediaFacade = mock(MediaFacade.class);
        CarePlanVerifier carePlanVerifier = mock(CarePlanVerifier.class);
        doReturn(true).when(carePlanVerifier).isActiveCarePlan(anyObject());
        ResourceConsolidator resourceConsolidator = new ResourceConsolidator(patientResolver, deviceResolver, mediaFacade, carePlanVerifier);
        FhirInputService.setResourceHandler(new ResourceHandler(resourceConsolidator));

        Identifier ptIdentifier = new Identifier()
                .setSystem(System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM"))
                .setValue("2512489996");
        Reference patientRef = new Reference().setIdentifier(ptIdentifier).setType("Patient");
        Identifier devIdentifier = new Identifier()
                .setSystem(System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"))
                .setValue("MDC1234");
        Reference deviceRef = new Reference().setIdentifier(devIdentifier).setType("Device");

        when(patientResolver.getReference(any())).thenReturn(patientRef);
        when(deviceResolver.getReference(any())).thenReturn(deviceRef);
    }

    private Resource createResource(String testFileName, Topic expectedOutputTopic) throws IOException, ParseException,
            MessageParseException, InterruptedException, ExecutionException, TimeoutException {
        String resourceString = ResourceUtil.stringFromResource(testFileName);
        Resource resource = (Resource) jsonParser.parseResource(resourceString);

        //Add correlationId header to client request
        CorrelationIdInterceptor correlationIdInterceptor = new CorrelationIdInterceptor(MessagingUtils.verifyOrCreateId(null));
        ourClient.registerInterceptor(correlationIdInterceptor);

        MethodOutcome methodOutcome = ourClient.create().resource(resource).prefer(PreferReturnEnum.OPERATION_OUTCOME).execute();
        assertTrue(methodOutcome.getCreated());
        OperationOutcome operationOutcome = (OperationOutcome) methodOutcome.getOperationOutcome();
        assertTrue(operationOutcome.hasIssue());
        assertTrue(operationOutcome.getIssue().get(0).hasSeverity());
        assertEquals("information", operationOutcome.getIssue().get(0).getSeverityElement().getValueAsString());

        Future<Message> future =
                ((MockKafkaEventProducer)FhirInputService.eventProducer).getTopicFuture(expectedOutputTopic);
        Message outputMessage = future.get(5000, TimeUnit.MILLISECONDS);

        ourLog.debug("testCreate - output message:\n" + outputMessage.toString());

        return (Resource) jsonParser.parseResource(outputMessage.getBody());
    }

    private void testPatientRef(Reference ref) {
        assertTrue(ref.getType().equals("Patient"));
        assertTrue(ref.getIdentifier().getSystem().equals(System.getenv(
                "OFFICIAL_PATIENT_IDENTIFIER_SYSTEM")));
        assertTrue(ref.getIdentifier().getValue().equals("2512489996"));
    }

    @AfterClass
	public static void afterClass() throws Exception {
		ourServer.stop();
	}

	@BeforeClass
	public static void beforeClass() throws Exception {

        jsonParser = FhirInputService.getMyFhirContext().newJsonParser();
        jsonParser.setPrettyPrint(true);
        /*
		 * This runs under maven, and I'm not sure how else to figure out the target directory from code..
		 */
		String path = FhirInputServiceTest.class.getClassLoader().getResource(".keep").getPath();
		path = new File(path).getParent();
		path = new File(path).getParent();
		path = new File(path).getParent();

		ourLog.info("Project base path is: {}", path);

        TestUtils.readEnvironment(path + "/" + ENVIRONMENT_FILE, environmentVariables);
        TestUtils.readEnvironment(path + "/" + DEPLOYMENT_FILE, environmentVariables);

        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");

        //Init web server and start it
        ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer(path+"/src/main/webapp/WEB-INF/without-keycloak/web.xml",path+"/target/service", environmentVariables, ourPort, ourServer);

		//Mock the KafkaProducer
        FhirInputService.eventProducer = new MockKafkaEventProducer(System.getenv("SERVICE_NAME"));

        ourServerBase = "http://localhost:" + ourPort + "/baseR4";
        ourClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(ourServerBase, FhirInputService.getMyFhirContext());
		ourClient.registerInterceptor(new LoggingInterceptor(true));
	}

    public class CorrelationIdInterceptor implements IClientInterceptor {

        public static final String HEADER_CORRELATION_ID = "CorrelationId";

        private String correlationId;

        public CorrelationIdInterceptor(String correlationId) {
            super();
            this.correlationId = correlationId;
        }

        @Override
        public void interceptRequest(IHttpRequest theRequest) {
            if(correlationId != null) theRequest.addHeader(HEADER_CORRELATION_ID, correlationId);
        }

        @Override
        public void interceptResponse(IHttpResponse theResponse) throws IOException {
            // nothing
        }
    }
}

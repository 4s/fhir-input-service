package dk.s4.microservices.fhirinputservice.auditlog;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import dk.s4.microservices.microservicecommon.security.DefaultUserContextResolver;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DiasAuditLogInterceptorTest {
    private DiasSender sender;
    private String diasUrl;
    private JSONParser parser;
    private final String serviceId = "fhir-input-service";
    private String expectedHeaders;
    private String expectedParameters;

    @Before
    public void Setup(){
        expectedHeaders = "X-Original-Uri: header1, Referer: header2";
        expectedParameters = "param1: [p1, p2], param2: [p3, p4]";
    }

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    @Test
    public void TestPractitionerPOST() throws ParseException, UserContextResolverInterface.UserContextResolverException {
        String operationType = "POST";
        String userId = "someUserId";
        String organization = "453191000016003";
        String requestUrl = "/Patient";
        String correlationId = "correlationId";

        HttpServletRequest requestMock = getRequestMock(requestUrl, operationType, correlationId);
        HttpServletResponse responseMock = getHttpResponseMock();
        DefaultUserContextResolver userContextResolverMock = getUserContextResolverMock(userId, organization);

        DiasAuditLogInterceptorAdaptor interceptorAdaptor = new DiasAuditLogInterceptorAdaptor(userContextResolverMock, sender);
        interceptorAdaptor.incomingRequestPreProcessed(requestMock, responseMock);

        LoggedRequest outgoingRequest = getValidatedOutgoingRequest();
        validateOutgoingRequest(outgoingRequest, correlationId);

        JSONObject body = (JSONObject) parser.parse(outgoingRequest.getBodyAsString());
        JSONObject messageOutObj = (JSONObject) parser.parse(body.get("message").toString());
        validateMessage(messageOutObj, userId, organization, operationType, requestUrl);
    }

    @Test
    public void TestPatientGET() throws ParseException, UserContextResolverInterface.UserContextResolverException {
        String operationType = "GET";
        String userId = "someUserId";
        String organization = "";
        String requestUrl = "/Patient";
        String correlationId = "correlationId";

        HttpServletRequest requestMock = getRequestMock(requestUrl, operationType, correlationId);
        HttpServletResponse responseMock = getHttpResponseMock();
        DefaultUserContextResolver userContextResolverMock = getUserContextResolverMock(userId, organization);

        DiasAuditLogInterceptorAdaptor interceptorAdaptor = new DiasAuditLogInterceptorAdaptor(userContextResolverMock, sender);
        interceptorAdaptor.incomingRequestPreProcessed(requestMock, responseMock);

        LoggedRequest outgoingRequest = getValidatedOutgoingRequest();
        validateOutgoingRequest(outgoingRequest, correlationId);

        JSONObject body = (JSONObject) parser.parse(outgoingRequest.getBodyAsString());
        JSONObject messageOutObj = (JSONObject) parser.parse(body.get("message").toString());
        validateMessage(messageOutObj, userId, organization, operationType, requestUrl);
    }

    @Test
    public void TestPatientGETNoHeadersOrParams() throws ParseException, UserContextResolverInterface.UserContextResolverException {
        String operationType = "GET";
        String userId = "someUserId";
        String organization = "";
        String requestUrl = "/Patient";
        String correlationId = "correlationId";

        HttpServletRequest requestMock = getRequestMock(requestUrl, operationType, correlationId);
        when(requestMock.getHeader("X-Original-Uri")).thenReturn(null);
        when(requestMock.getHeader("Referer")).thenReturn(null);
        when(requestMock.getParameterMap()).thenReturn(new HashMap<>());

        HttpServletResponse responseMock = getHttpResponseMock();
        DefaultUserContextResolver userContextResolverMock = getUserContextResolverMock(userId, organization);

        DiasAuditLogInterceptorAdaptor interceptorAdaptor = new DiasAuditLogInterceptorAdaptor(userContextResolverMock, sender);
        interceptorAdaptor.incomingRequestPreProcessed(requestMock, responseMock);

        LoggedRequest outgoingRequest = getValidatedOutgoingRequest();
        validateOutgoingRequest(outgoingRequest, correlationId);

        expectedHeaders = "X-Original-Uri: null, Referer: null";
        expectedParameters = "";

        JSONObject body = (JSONObject) parser.parse(outgoingRequest.getBodyAsString());
        JSONObject messageOutObj = (JSONObject) parser.parse(body.get("message").toString());
        validateMessage(messageOutObj, userId, organization, operationType, requestUrl);
    }

    private void validateMessage(JSONObject messageOutObj, String userId, String organization, String operationType, String requestUrl){
        Assert.assertEquals(serviceId, messageOutObj.get("ServiceId"));
        Assert.assertEquals(userId, messageOutObj.get("UserId"));
        Assert.assertEquals(organization, messageOutObj.get("Organization"));
        Assert.assertEquals(operationType, messageOutObj.get("OperationType"));
        Assert.assertEquals(requestUrl, messageOutObj.get("HTTPRequest").toString());
        Assert.assertEquals(expectedHeaders, messageOutObj.get("Headers"));
        Assert.assertEquals(expectedParameters, messageOutObj.get("Parameters"));
    }

    private void validateOutgoingRequest(LoggedRequest outgoingRequest, String correlationId){
        Assert.assertEquals(diasUrl, outgoingRequest.getAbsoluteUrl());
        Assert.assertEquals("", outgoingRequest.getHeader("SESSION"));
        Assert.assertEquals(correlationId, outgoingRequest.getHeader("correlation-id"));
    }

    private LoggedRequest getValidatedOutgoingRequest() {
        Assert.assertEquals(1, getAllServeEvents().size());
        ServeEvent auditEvent = getAllServeEvents().get(0);
        Assert.assertNotNull(auditEvent);
        Assert.assertEquals(201, auditEvent.getResponse().getStatus());
        return auditEvent.getRequest();
    }

    private HttpServletResponse getHttpResponseMock() {
        return mock(HttpServletResponse.class);
    }

    private HttpServletRequest getRequestMock(String requestUrl, String operationType, String correlationId) {
        HttpServletRequest requestMock = mock(HttpServletRequest.class);
        when(requestMock.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        when(requestMock.getMethod()).thenReturn(operationType);
        when(requestMock.getHeader("HEADER_CORRELATION_ID")).thenReturn(correlationId);
        when(requestMock.getParameterMap()).thenReturn(createTestParameters());

        List<String> headerList = Arrays.asList("header1", "header2");
        when(requestMock.getHeaderNames()).thenReturn(Collections.enumeration(headerList));
        when(requestMock.getHeader("X-Original-Uri")).thenReturn(headerList.get(0));
        when(requestMock.getHeader("Referer")).thenReturn(headerList.get(1));

        return requestMock;
    }

    private Map<String, String[]> createTestParameters(){
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put("param1", new String[]{"p1", "p2"});
        parameters.put("param2", new String[]{"p3", "p4"});
        return parameters;
    }

    private DefaultUserContextResolver getUserContextResolverMock(String userId, String organization)
            throws UserContextResolverInterface.UserContextResolverException {
        DefaultUserContextResolver userContextResolverMock = mock(DefaultUserContextResolver.class);
        when(userContextResolverMock.getUserId(anyObject())).thenReturn(userId);
        when(userContextResolverMock.getUserOrganization(anyObject())).thenReturn(organization);
        return userContextResolverMock;
    }

    @Before
    public void before() {
        diasUrl = setupWireMock() + "/audit";
        sender = new DiasSender(setupWireMock() + "/audit", serviceId);
        parser = new JSONParser();
    }

    private String setupWireMock() {
        String url = "http://localhost:" + wireMockRule.port();

        stubFor(post(urlEqualTo("/audit")).atPriority(1)
                .willReturn(aResponse().withStatus(201)));

        return url;
    }
}
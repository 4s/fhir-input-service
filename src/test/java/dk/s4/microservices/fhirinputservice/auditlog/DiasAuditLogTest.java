package dk.s4.microservices.fhirinputservice.auditlog;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class DiasAuditLogTest {

    private Map<String, String> headers;
    private String expectedHeaderValue;
    private String expectedHttpRequest;
    private String expectedOperationType;
    private String expectedOrganization;
    private String expectedUserId;

    @Before
    public void setup(){
        headers = new HashMap<>();
        headers.put("header1", "value1");
        headers.put("header2", "value2");
    }

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    @Test
    public void testDiasSender() throws Exception {

        expectedHttpRequest = "someHttpRequest";
        expectedOperationType = "POST";
        expectedOrganization = "someOrganization";
        expectedUserId = "someUserId";
        expectedHeaderValue = "header2: value2, header1: value1";

        DiasAuditLogMessage message = new DiasAuditLogMessage()
                .withHTTPRequest(expectedHttpRequest)
                .withOperationType(expectedOperationType)
                .withOrganization(expectedOrganization)
                .withUserId(expectedUserId)
                .withHeaders(headers)
                .finish();

        testDiasSender(message);

    }

    @Test
    public void testDiasSenderNoValues()throws Exception {

        expectedHttpRequest = null;
        expectedOperationType = null;
        expectedOrganization = null;
        expectedUserId = null;
        expectedHeaderValue = null;

        DiasAuditLogMessage message = new DiasAuditLogMessage().finish();

        testDiasSender(message);
    }

    private void testDiasSender(DiasAuditLogMessage message) throws Exception{
        DiasSender diasSender = new DiasSender(setupWireMock(201) + "/audit" , "fhir-input-service");
        diasSender.sendToAuditLog("someSessionId", "someCorrelationId",
                Instant.ofEpochMilli(Long.parseLong("1603715029379")), message);

        LoggedRequest request = getAllServeEvents().get(0).getRequest();

        Assert.assertEquals("someSessionId", request.getHeader("SESSION"));
        Assert.assertEquals("someCorrelationId", request.getHeader("correlation-id"));

        JSONParser parser = new JSONParser();
        JSONObject body = (JSONObject) parser.parse(request.getBodyAsString());

        // The timestamp should follow the RFC3339 date format
        Assert.assertEquals("2020-10-26T12:23:49.379Z", body.get("timestamp").toString());

        JSONObject messageOutObj = (JSONObject) parser.parse(body.get("message").toString());
        Assert.assertEquals("fhir-input-service", messageOutObj.get("ServiceId"));
        Assert.assertEquals(expectedUserId, messageOutObj.get("UserId"));
        Assert.assertEquals(expectedOrganization, messageOutObj.get("Organization"));
        Assert.assertEquals(expectedOperationType, messageOutObj.get("OperationType"));
        Assert.assertEquals(expectedHttpRequest, messageOutObj.get("HTTPRequest"));
        Assert.assertEquals(expectedHeaderValue, messageOutObj.get("Headers"));
    }

    @Test (expected = IOException.class)
    public void testDiasSenderFail() throws Exception {

        DiasAuditLogMessage message = new DiasAuditLogMessage()
                .withHTTPRequest("someHttpRequest")
                .withOperationType("POST")
                .withOrganization("someOrganization")
                .withUserId("someUserId")
                .withHeaders(headers)
                .finish();

        DiasSender diasSender = new DiasSender(setupWireMock(500) + "/audit" , "fhir-input-service");

        diasSender.sendToAuditLog("someSessionId", "someCorrelationId",
                Instant.ofEpochMilli(Long.parseLong("1603715029379")), message);
    }

    private String setupWireMock(int expectedReturnCode) {

        stubFor(post(urlEqualTo("/audit")).atPriority(1)
                .willReturn(aResponse().withStatus(expectedReturnCode)));

        return "http://localhost:" + wireMockRule.port();
    }
}

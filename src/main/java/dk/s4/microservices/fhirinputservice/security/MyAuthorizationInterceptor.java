package dk.s4.microservices.fhirinputservice.security;

import java.util.List;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.server.interceptor.auth.AuthorizationInterceptor;
import ca.uhn.fhir.rest.server.interceptor.auth.IAuthRule;
import ca.uhn.fhir.rest.server.interceptor.auth.RuleBuilder;
//import dk.s4.microservices.genericresourceservice.security.AccessTokenParser;
//import dk.s4.microservices.genericresourceservice.utils.Utils;


/**
 * 
 * The AuthorizationInterceptor works by allowing you to declare permissions based on an individual request coming in. 
 * In other words, you could have code that examines an incoming request and determines that it is being made by a Patient with ID 123. 
 * You could then declare that the requesting user has access to read and write any resource in compartment "Patient/123", 
 * which corresponds to any Observation, MedicationOrder etc with a subject of "Patient/123".
 *
 */
public class MyAuthorizationInterceptor extends AuthorizationInterceptor {
	
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MyAuthorizationInterceptor.class);
	
	private IdDt resourceId = null;
	private boolean isAdmin = false; 
	
	public MyAuthorizationInterceptor() {
	}
	
	 @Override
	 public List<IAuthRule> buildRuleList(RequestDetails theRequestDetails) {

/*
		 IdDt userId = null;

		 // Get the Keycloak access token from the header
		 String token = Utils.getToken(theRequestDetails);
		 if (token.isEmpty()) {
			 LOG.info("The received authHeader is blank. Deny access to resources");
			 return new RuleBuilder()
					 .denyAll("Anonymous read and write access denied on this FHIR server")
					 .build();
		 }
		 LOG.debug("Access token received = " + token);


		 // Get the global Realm roles from the access token. If the list of roles contain the "admin" role, the user is an administrator.
		 // Or if the list of roles contain the "clinician" role, get the UserId from the access token and use the ID to create authorization rules for clinicians.
		 // Or if the list of roles contain the "patient" role, get the UserId from the access token, and use the ID to create authorization rules for patients.
		 // The UserId is delivered as a Custom User Attribute in the access token.

		 JsonArray roles = AccessTokenParser.getGlobalRoles(token);

		 if (roles != null) {

			 if (AccessTokenParser.isAdmin(roles)) {
				 // This user is an administrator and has access to everything
				 LOG.debug("The user is admin");
				 return new RuleBuilder()
						 .allowAll("Rule for admin: allow all")
						 .build();
			 } else if (AccessTokenParser.isClinician(roles)) {
				 userId = new IdDt("Practitioner",
						 AccessTokenParser.getAttributeValueFromToken(token, "UserId"));
				 LOG.debug("The user is a clinician. The UserId = " + userId);

				 //If the user is an clinician, deny access - fHIR Input service is only directed towards patients
				 return new RuleBuilder()
						 .denyAll("Access denied to the resource in this service for the given user")
						 .build();
			 } else if (AccessTokenParser.isPatient(roles)) {
				 // This user has access only to resources of the form: "Patient/ID".
				 // If the user is a patient with a UserId, we create the following rule chain:
				 //  - Allow the user to write anything in their own compartment.

				 userId = new IdDt("Patient", AccessTokenParser.getAttributeValueFromToken(token, "UserId"));
				 LOG.debug("The user is a patient. The UserId = " + userId);
				 List<IAuthRule> result = new RuleBuilder()
						 .allow("Write access for patient with ID").write().allResources().inCompartment("Patient", userId).andThen()
						 .denyAll("Access denied to the resource in this service for the given user")
						 .build();
				 return result;
			 }
		 }
*/

		 // As default, deny everything.
		 return new RuleBuilder()
				 .allow("Write access").write().allResources().withAnyId().andThen()
				 .denyAll("Deny all access to resource in this service for the given user")
				 .build();
	 }
}

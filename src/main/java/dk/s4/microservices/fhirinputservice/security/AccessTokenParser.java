package dk.s4.microservices.fhirinputservice.security;

import java.util.Iterator;

import javax.xml.bind.DatatypeConverter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class AccessTokenParser {
	
	/**
	 * Parse the access token which is in JSON format and return the roles associated.
	 * @param accessToken Access token in JSON format
	 * @return a JSONArray containing the roles for the current user for this service!
	 */
	public static JSONArray getRoles(String accessToken) {
		
		JSONArray result = null;
		
		// Split the access token and get the payload. 
		String[] payloadEncodedArray = accessToken.split("\\.");
		
		if (payloadEncodedArray.length > 1) {
			String payloadEncoded = payloadEncodedArray[1];
		
			// Then, base64 decode the payload
			String payload = new String(DatatypeConverter.parseBase64Binary(payloadEncoded));
			
			// This is a hack... sometimes, the result after parsing and decoding removed the end bracket from the JSON string
			if (payload.endsWith("}") == false) {
				payload = payload + "}";
			}
			
			// Parse the payload as JSON
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = null;
			try {
				jsonObject = (JSONObject) parser.parse(payload);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	
			if (jsonObject != null) {
		        System.out.println(jsonObject);
		        
		        if (jsonObject.containsKey("resource_access")) {
		        	
		        	JSONObject resource_access = (JSONObject) jsonObject.get("resource_access");
		        	System.out.println(resource_access);
		        	
		        	if (resource_access.containsKey("fhir-input-service")) {
		        		JSONObject resource  = (JSONObject) resource_access.get("fhir-input-service");
		        		
		        		if (resource.containsKey("roles")) {
		        			result = (JSONArray) resource.get("roles");
		        		}
		        	}
		        }
			}
			else {
				System.out.println("jsonObject for roles was null...");
			}
		}
		
		return result;
	}
	
	/**
	 * Parse the access token which is in JSON format and return the value of the PatientId attribute.
	 * @param accessToken Access token in JSON format
	 * @return the value of the PatiendId attribute
	 */
	public static String getPatientId(String accessToken) {
		
		String patientId = null;
		
		// Split the access token and get the payload. 
		String[] payloadEncodedArray = accessToken.split("\\.");
		
		if (payloadEncodedArray.length > 1) {
			String payloadEncoded = payloadEncodedArray[1];
		
			// Then, base64 decode the payload
			String payload = new String(DatatypeConverter.parseBase64Binary(payloadEncoded));
			
			// This is a hack... sometimes, the result after parsing and decoding removed the end bracket from the JSON string
			if (payload.endsWith("}") == false) {
				payload = payload + "}";
			}
			
			// Parse the payload as JSON
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = null;
			try {
				jsonObject = (JSONObject) parser.parse(payload);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	
			if (jsonObject != null) {
		        System.out.println(jsonObject);
		        
		        if (jsonObject.containsKey("PatientId")) {
		        	System.out.println("has key PatientId");
		        	patientId = (String) jsonObject.get("PatientId");
		        }
			}
			else {
				System.out.println("jsonObject for PatientId was null...");
			}
		
		}
		
		return patientId;
	}
	
	/**
	 * Checks whether the current user has been assigned the "admin" role.
	 * @param roles 
	 * 			the list of roles assigned to the current user for this service.
	 * @return 
	 * 		true, if the user is an administrator.
	 */
	public static boolean isAdmin(JSONArray roles) {
		boolean result = false;
		
        Iterator it = roles.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            String role = (String) obj.toString();
            if (role.equalsIgnoreCase("admin")) {
            	result = true;
            	System.out.println("Found role: " + role);
            }
        }
        return result;
	}
	
	/**
	 * Checks if "patient" is one of the roles in the supplied JSONArray
	 * @param roles JSONArray of roles
	 * @return true if "patient" is one of the roles
	 */
	public static boolean isPatient(JSONArray roles) {
		boolean result = false;
		
        Iterator it = roles.iterator();
        while (it.hasNext()) {
        	Object obj = it.next();
            String role = (String) obj.toString();
            if (role.equalsIgnoreCase("patient")) {
            	result = true;
            	System.out.println("Found role: " + role);
            }
            
        }
        return result;
	}
	
}

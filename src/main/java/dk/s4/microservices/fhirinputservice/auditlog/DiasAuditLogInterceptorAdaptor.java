package dk.s4.microservices.fhirinputservice.auditlog;

import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import dk.s4.microservices.microservicecommon.security.ThreadLocalContext;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * FHIR {@link InterceptorAdapter} subclass that handles the authentication aspects of http requests that are relevant only for DIAS authentication
 */
public class DiasAuditLogInterceptorAdaptor extends InterceptorAdapter {

    private final UserContextResolverInterface userContextResolver;

    private final DiasSender diasSender;

    public DiasAuditLogInterceptorAdaptor(UserContextResolverInterface userContextResolver, DiasSender diasSender ) {
        this.userContextResolver = userContextResolver;
        this.diasSender = diasSender;
    }

    @Override
    public boolean incomingRequestPreProcessed(HttpServletRequest theRequest, HttpServletResponse theResponse) throws AuthenticationException {

        DiasAuditLogMessage message;

        try{
             message = new DiasAuditLogMessage()
                    .withHTTPRequest(theRequest.getRequestURL().toString())
                    .withOperationType(theRequest.getMethod())
                    .withUserId(userContextResolver.getUserId(theRequest))
                    .withHeaders(getHeaders(theRequest))
                     .withParameters(theRequest.getParameterMap())
                     .withOrganization(userContextResolver.getUserOrganization(theRequest))
                    .finish();
        }
        catch (UserContextResolverInterface.UserContextResolverException e){
            return false;
        }

        try{
            this.diasSender.sendToAuditLog(ThreadLocalContext.get("SESSION"), theRequest.getHeader("HEADER_CORRELATION_ID"), Instant.now(), message);
        }
        catch (IOException e){
            return false;
        }

        return true;

    }

    private Map<String, String> getHeaders(HttpServletRequest theRequest){
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Original-Uri", theRequest.getHeader("X-Original-Uri"));
        headers.put("Referer", theRequest.getHeader("Referer"));
        return headers;
    }

}

package dk.s4.microservices.fhirinputservice.auditlog;

import java.util.Map;

public class DiasAuditLogMessage {

    private String userId;
    private String organization;
    private String operationType;
    private String HTTPRequest;
    private Map<String, String> headers;
    private Map<String, String[]> parameters;

    public DiasAuditLogMessage withUserId(String userId){
        this.userId = userId;
        return this;
    }

    public DiasAuditLogMessage withHeaders(Map<String, String> headers){
        this.headers = headers;
        return this;
    }

    public DiasAuditLogMessage withOrganization(String organization){
        this.organization = organization;
        return this;
    }

    public DiasAuditLogMessage withOperationType(String operationType){
        this.operationType = operationType;
        return this;
    }

    public DiasAuditLogMessage withHTTPRequest(String HTTPRequest){
        this.HTTPRequest = HTTPRequest;
        return this;
    }

    public DiasAuditLogMessage withParameters(Map<String, String[]> parameters){
        this.parameters = parameters;
        return this;
    }


    public DiasAuditLogMessage finish(){
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public String getOrganization() {
        return organization;
    }

    public String getOperationType() {
        return operationType;
    }

    public String getHTTPRequest() {
        return HTTPRequest;
    }

    public Map<String, String> getHeaders(){
        return headers;
    }

    public Map<String, String[]> getParameters(){
        return parameters;
    }

}

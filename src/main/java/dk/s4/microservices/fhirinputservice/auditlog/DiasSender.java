package dk.s4.microservices.fhirinputservice.auditlog;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Instant;
import java.util.stream.Collectors;

public class DiasSender {

    private static final Logger logger = LoggerFactory.getLogger(DiasSender.class);

    private final String url;
    private final String serviceId;

    public DiasSender(String url, String serviceId){
        this.url = url;
        this.serviceId = serviceId;
    }

    /**
     The method throws an IOException if the message is not successfully delivered.
    */
    public void sendToAuditLog(String session, String correlationId, Instant instant, DiasAuditLogMessage diasAuditLog) throws IOException {
        JSONObject body = new JSONObject();
        body.put("timestamp", instant.toString());
        body.put("message", buildMessage(diasAuditLog).toJSONString());

        HttpPost request = new HttpPost(url);
        request.addHeader("SESSION",  session);
        request.addHeader("correlation-id", correlationId);

        request.setEntity(new StringEntity(body.toString()));

        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = client.execute(request);
        client.close();

        logger.debug("AuditLog: " + request + ", Response: " + response.getStatusLine());

        if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201){
            throw new IOException("DiasSender failed: " + response.getStatusLine());
        }
    }

    private JSONObject buildMessage(DiasAuditLogMessage auditLog){
        JSONObject messageJSON = new JSONObject();
        messageJSON.put("ServiceId", this.serviceId);
        messageJSON.put("UserId", auditLog.getUserId());
        messageJSON.put("Organization", auditLog.getOrganization());
        messageJSON.put("OperationType", auditLog.getOperationType());
        messageJSON.put("HTTPRequest", auditLog.getHTTPRequest());

        if (auditLog.getParameters() != null){
            String params = auditLog.getParameters().entrySet().stream().map( (e) ->
                    e.getKey() + ": [" + String.join(", ",  e.getValue()) + "]"
            ).collect(Collectors.joining(", "));
            messageJSON.put("Parameters", params);
        }

        if (auditLog.getHeaders() != null){
            messageJSON.put("Headers", auditLog.getHeaders().entrySet().stream().map( (e) ->  e.getKey() + ": " + e.getValue() ).collect(Collectors.joining(", ")));
        }
        return messageJSON;
    }

}

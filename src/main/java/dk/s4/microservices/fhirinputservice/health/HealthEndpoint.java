package dk.s4.microservices.fhirinputservice.health;

import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import org.json.simple.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class HealthEndpoint {

    static private KafkaConsumeAndProcess kafkaConsumeAndProcess;

    /**
     * Web service endpoint for checking overall health of the service.
     *
     */
    @GET
    @Path("/")
    public Response getMsg() {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "all good");
        return Response.ok(jsonObject.toJSONString(), MediaType.APPLICATION_JSON).build();

    }

}

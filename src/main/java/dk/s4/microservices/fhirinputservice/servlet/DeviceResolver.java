package dk.s4.microservices.fhirinputservice.servlet;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.ReplyFuture;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.security.ThreadLocalContext;
import java.sql.Ref;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Contains functionality in relation to referencing and creating Device resources
 */
public class DeviceResolver implements ResolverInterface {
    private static final Logger logger = LoggerFactory.getLogger(DeviceResolver.class);

    /**
     * From a Bundle local Device resource get a logical reference based on the official identifier system.
     *
     * @param bundleLocalResource Bundle local Device resource
     * @return Logical reference to Device in the form of a HAPIFHIR Reference object.
     */
    public Reference getReference(IBaseResource bundleLocalResource) {
        logger.debug("getReference");
        Identifier localIdentifier = new Identifier();
        for (Identifier identifier: ((Device) bundleLocalResource).getIdentifier()) {
            if (identifier.hasSystem()
                    && identifier.hasValue()
                    && identifier.getSystem().equals(System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"))) {
                localIdentifier = identifier;
                logger.debug("Device identifier: system = {} value = {}", localIdentifier.getSystem(),
                        localIdentifier.getValue());
            }
        }
        if (!localIdentifier.hasSystem())
            throw ExceptionUtils.fatalFormatException("Device doesn not have required identifier with system = "
                    + System.getenv("OFFICIAL_DEVICE_IDENTIFIER_SYSTEM"));

        return new Reference().setIdentifier(localIdentifier).setType("Device");
    }

    /**
     * Emit event over messaging system to conditionally create Device resource.
     *
     * @param device FHIR Device resource
     * @param reference logical reference to the device
     */
    public void createConditionally(Device device, Reference reference) {
        Message message = new Message().
                setSender(System.getenv("SERVICE_NAME")).
                setBodyCategory(Message.BodyCategory.FHIR).
                setBodyType("Device").
                setContentVersion(System.getenv("FHIR_VERSION")).
                setCorrelationId(MDC.get(System.getenv("CORRELATION_ID"))).
                setTransactionId(MDC.get("transactionId")).
                setSession(ThreadLocalContext.get("SESSION")).
                setSecurity(ThreadLocalContext.get("SECURITY_TOKEN")).
                setIfNoneExist("identifier=" + reference.getIdentifier().getSystem() + "|" + reference.getIdentifier().getValue()).
                setBody(FhirInputService.getMyFhirContext().newJsonParser().encodeResourceToString(device));
        Topic topic = new Topic().
                setOperation(Topic.Operation.Create).
                setDataCategory(Topic.Category.FHIR).
                setDataType(device.fhirType());
        FhirInputService.eventProducer.sendMessage(topic, message);
    }

}

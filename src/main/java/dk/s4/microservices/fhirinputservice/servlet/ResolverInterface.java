package dk.s4.microservices.fhirinputservice.servlet;

import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Reference;

public interface ResolverInterface {
    public Reference getReference(IBaseResource bundleLocalResource) throws UnprocessableEntityException, InternalErrorException;
}

package dk.s4.microservices.fhirinputservice.servlet;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains functionality to take a Bundle local Patient resource and use the identifier of this to get the Patient
 * resource from another FHIR service and construct a logical reference based .
 */
public class PatientResolver implements ResolverInterface {
    private static final Logger logger = LoggerFactory.getLogger(PatientResolver.class);

    /**
     * From a Bundle local Patient resource get a logical reference based on the official identifier system.
     *
     * @param bundleLocalResource Bundle local Patient resource
     * @return Full url reference to Patient in the form of a HAPIFHIR Reference object.
     * @throws ResourceNotFoundException when searching for the resource on service at PATIENTCARE_SERVICE_URL yields no
     * results or fails entirely (for instance when not being able to reach PATIENTCARE_SERVICE_URL).
     */
    @Override
    public Reference getReference(IBaseResource bundleLocalResource)
            throws UnprocessableEntityException, InternalErrorException {
        try {
            logger.debug("getReference");
            Identifier localIdentifier = new Identifier();
            for (Identifier identifier: ((Patient) bundleLocalResource).getIdentifier()) {
                if (identifier.hasSystem() && identifier.hasValue()) {
                    localIdentifier = identifier;
                    logger.trace("Patient identifier: system = {} value = {}", localIdentifier.getSystem(),
                                 localIdentifier.getValue());
                }
            }
            if (!localIdentifier.hasSystem())
                throw ExceptionUtils.fatalFormatException("Patient identifier with system and value not found");

            IGenericClient patientClient = MyFhirClientFactory.getInstance()
                                                              .getClientFromBaseUrl(System.getenv("PATIENTCARE_SERVICE_URL"), FhirInputService.getMyFhirContext());

            String searchUrl = System.getenv("PATIENTCARE_SERVICE_URL") +
                    "/Patient?_query=searchByIdentifier&" +
                    "identifier=" + localIdentifier.getSystem() + "|" + localIdentifier.getValue();
            Bundle bundle = patientClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

            if (!bundle.hasEntry() || bundle.getEntry().isEmpty())
                throw ExceptionUtils.fatalInternalErrorException("Failed patient lookup on url: " + searchUrl);

            Identifier officialIdentifier = null;
            Patient lookedUpPatient = (Patient) bundle.getEntry().get(0).getResource();
            if (!lookedUpPatient.hasIdentifier())
                throw ExceptionUtils.fatalInternalErrorException("Found patient with no identifier - should not happen");
            for (Identifier identifier: lookedUpPatient.getIdentifier()) {
                if (identifier.hasSystem()
                        && identifier.getSystem().equals(System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM")))
                    officialIdentifier = identifier;
            }

            if (officialIdentifier == null)
                throw ExceptionUtils.fatalInternalErrorException("Found patient with no official identifier - should not " +
                                                                         "happen");

            return new Reference().setIdentifier(officialIdentifier).setType("Patient");
        }
        catch (FhirClientConnectionException e) {
            logger.error("error: " + e.getMessage() + " cause: " + e.getCause());
            throw ExceptionUtils.fatalInternalErrorException("Client connection failure in PatientResolver.getReference", e);
        }
    }
}

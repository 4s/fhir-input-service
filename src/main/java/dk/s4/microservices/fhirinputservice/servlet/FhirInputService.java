package dk.s4.microservices.fhirinputservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.StrictErrorHandler;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.CorsInterceptor;
import ca.uhn.fhir.rest.server.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import dk.s4.microservices.fhirinputservice.auditlog.DiasAuditLogInterceptorAdaptor;
import dk.s4.microservices.fhirinputservice.auditlog.DiasSender;
import dk.s4.microservices.fhirinputservice.provider.FHIRBundleResourceProvider;
import dk.s4.microservices.fhirinputservice.provider.FHIRObservationResourceProvider;
import dk.s4.microservices.fhirinputservice.provider.ResourceHandler;
import dk.s4.microservices.fhirinputservice.security.MyAuthorizationInterceptor;
import dk.s4.microservices.messaging.EventProducer;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.Env;
import dk.s4.microservices.microservicecommon.fhir.CarePlanVerifier;
import dk.s4.microservices.microservicecommon.fhir.DiasInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.fhir.MyInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.security.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.cors.CorsConfiguration;

/**
 * The FHIR Input Service exposes a RESTful interface for delivery of
 * FHIR Observation resources.
 * 
 * When receiving FHIR Observation resources, this service writes the same FHIR
 * resource directly on Kafka as the payload (JSON encoded) - on a topic named
 * "observation"
 * 
 */
public class FhirInputService extends RestfulServer {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(FhirInputService.class);

	/** Singleton FhirContext
	 *
	 * @return the FhirContext
	 */
	public static FhirContext getMyFhirContext() {
    	if (fhirContext == null) {
			fhirContext = FhirContext.forR4();
			fhirContext.setParserErrorHandler(new StrictErrorHandler());
		}
		return fhirContext;
	}

    private static FhirContext fhirContext;

	private UserContextResolverInterface contextResolver;

	public static EventProducer eventProducer;

	public static void setResourceHandler(ResourceHandler resHandler) {
		resourceHandler = resHandler;
		bundleResourceProvider.setResourceHandler(resHandler);
		observationResourceProvider.setResourceHandler(resHandler);
	}

	private static ResourceHandler resourceHandler;
	private static FHIRBundleResourceProvider bundleResourceProvider;
	private static FHIRObservationResourceProvider observationResourceProvider;
    /**
	 * Constructor
	 */
	public FhirInputService() {
		super(getMyFhirContext());
		registerAndCheckEnvirontmentVars();
		// Register adapter that handles metrics, correlation-ids, authentication etc.
		if (Env.isSetToTrue("ENABLE_DIAS_AUTHENTICATION")) {
			contextResolver = new DiasUserContextResolver(System.getenv("USER_CONTEXT_SERVICE_URL"));
		} else if (Env.isSetToTrue("ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION")) {
			contextResolver = new KeycloakGatekeeperUserContextResolver();
		} else if (Env.isSetToTrue("ENABLE_OAUTH2_PROXY_AUTHORIZATION")) {
			contextResolver = new OAuth2ProxyUserContextResolver();
		} else {
			contextResolver = new DefaultUserContextResolver();
		}
	}

	private void registerAndCheckEnvirontmentVars() {
		Env.registerRequiredEnvVars(Arrays.asList(
				"SERVICE_NAME",
				"FHIR_VERSION",
				"CORRELATION_ID",
				"TRANSACTION_ID",
				"LOG_LEVEL",
				"LOG_LEVEL_DK_S4",
				"ENABLE_KAFKA",
				"SERVER_BASE_URL",
				"PATIENTCARE_SERVICE_URL",
				"OFFICIAL_PATIENT_IDENTIFIER_SYSTEM",
				"OFFICIAL_DEVICE_IDENTIFIER_SYSTEM",
				"FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS",
				"FHIRINPUTSERVICE_CODING_SYSTEMS_SHORTNAMES",
				"PHG_OBSERVATION_EXTENSION_URL",
				"PHG_QUESTIONNAIRERESPONSE_EXTENSION_URL",
				"MEDIA_CREATE_SEPARATELY",
				"ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION",
				"ENABLE_OAUTH2_PROXY_AUTHORIZATION"
				));
		Env.registerConditionalEnvVars("ENABLE_KAFKA",
				Arrays.asList(
						"KAFKA_BOOTSTRAP_SERVER",
						"KAFKA_GROUP_ID",
						"KAFKA_ACKS",
						"KAFKA_RETRIES",
						"KAFKA_KEY_SERIALIZER",
						"KAFKA_VALUE_SERIALIZER"));
		Env.registerConditionalEnvVars("ENABLE_DIAS_AUTHENTICATION",
				Arrays.asList("USER_CONTEXT_SERVICE_URL"));
		Env.registerConditionalEnvVars("ENABLE_AUTH",
				Arrays.asList("KEYCLOAK_REALM",
						"KEYCLOAK_CLIENT_NAME",
						"AUTH_SERVER_URL"));
		Env.checkEnv();
	}

	private void registerInterceptors(UserContextResolverInterface contextResolver) {
		/*
		 * Enable CORS
		 */
		CorsConfiguration config = new CorsConfiguration();
		CorsInterceptor corsInterceptor = new CorsInterceptor(config);
		config.addAllowedHeader("Origin");
		config.addAllowedHeader("Accept");
		config.addAllowedHeader("Prefer");
		config.addAllowedHeader("X-Requested-With");
		config.addAllowedHeader("Content-Type");
		config.addAllowedHeader("Access-Control-Request-Method");
		config.addAllowedHeader("Access-Control-Request-Headers");
		config.addAllowedHeader("Authorization");
		config.addAllowedHeader("SESSION");
		config.addAllowedOrigin("*");
		config.addExposedHeader("Location");
		config.addExposedHeader("Content-Location");
		config.setAllowedMethods(Arrays.asList("POST","OPTIONS"));
		registerInterceptor(corsInterceptor);

		MyInterceptorAdaptor myInterceptorAdaptor = new MyInterceptorAdaptor(contextResolver);
		registerInterceptor(myInterceptorAdaptor);

		if (Env.isSetToTrue("ENABLE_DIAS_AUTHENTICATION")) {
			DiasInterceptorAdaptor diasInterceptorAdaptor = new DiasInterceptorAdaptor(contextResolver);
			registerInterceptor(diasInterceptorAdaptor);
		}

		if (Env.isSetToTrue("ENABLE_AUTH")) {
			logger.debug("Initializing MyAuthorizationInterceptor");
			// Register the authorization interceptor
			MyAuthorizationInterceptor authzInterceptor = new MyAuthorizationInterceptor();
			this.registerInterceptor(authzInterceptor);
		}

		// Register HAPI FHIRs built in logging interceptor
		LoggingInterceptor loggingInterceptor = new LoggingInterceptor();
		registerInterceptor(loggingInterceptor);
		loggingInterceptor.setMessageFormat("operationType: ${operationType}\n"
													+ "operationName: ${operationName}\n"
													+ "idOrResourceName: ${idOrResourceName}\n"
													+ "requestParameters: ${requestParameters}\n"
													+ "requestUrl: ${requestUrl}\n"
													+ "requestVerb: ${requestVerb}\n"
													+ "processingTimeMillis: ${processingTimeMillis}"
										   );
		loggingInterceptor.setErrorMessageFormat("ERROR: ${exceptionMessage}\n"
														 + "operationType: ${operationType}\n"
														 + "operationName: ${operationName}\n"
														 + "idOrResourceName: ${idOrResourceName}\n"
														 + "requestParameters: ${requestParameters}\n"
														 + "requestUrl: ${requestUrl}\n"
														 + "requestVerb: ${requestVerb}\n"
														 + "processingTimeMillis: ${processingTimeMillis}"										   );
		loggingInterceptor.setLogger(logger);

		/*
		 * This server interceptor causes the server to return nicely formatter
		 * and coloured responses instead of plain JSON/XML if the request is
		 * coming from a browser window. It is optional, but can be nice for
		 * testing.
		 */
		registerInterceptor(new ResponseHighlighterInterceptor());

		if (Env.isSetToTrue("DIAS_AUDIT_ENABLED")){
			registerInterceptor(new DiasAuditLogInterceptorAdaptor(contextResolver,
					new DiasSender(System.getenv("DIAS_AUDIT_URL"),
							System.getenv("SERVICE_NAME"))));
		}

	}

	@Override
	public void initialize() throws ServletException {
        logger.debug("initialize");

		super.initialize();

		registerInterceptors(contextResolver);

		CarePlanVerifier carePlanVerifier = new CarePlanVerifier(System.getenv("PATIENTCARE_SERVICE_URL"),
				FhirInputService.getMyFhirContext(), MyFhirClientFactory.getInstance());

		//Check if null as tests may instantiate mock
		if (resourceHandler == null) {
			PatientResolver patientResolver = new PatientResolver();
			DeviceResolver deviceResolver = new DeviceResolver();
			MediaFacade mediaFacade = new MediaFacade();
			ResourceConsolidator resourceConsolidator = new ResourceConsolidator(patientResolver, deviceResolver, mediaFacade, carePlanVerifier);
			resourceHandler = new ResourceHandler(resourceConsolidator);
		}

		//Initialize resource providers
		List<IResourceProvider> providers = new ArrayList<IResourceProvider>();
		bundleResourceProvider = new FHIRBundleResourceProvider(resourceHandler);
		providers.add(bundleResourceProvider);
		observationResourceProvider = new FHIRObservationResourceProvider(resourceHandler);
		providers.add(observationResourceProvider);
		setResourceProviders(providers);

        if (Env.isSetToTrue("ENABLE_KAFKA")) {
			try {
				logger.debug("Initializing Kafka");
				eventProducer = new KafkaEventProducer(System.getenv("SERVICE_NAME"));
				if (System.getenv("ENABLE_DIAS_AUTHENTICATION").equals("true"))
					eventProducer.setInterceptors(new DiasEventProducerInterceptor());
			} catch (KafkaInitializationException e) {
				logger.error("Error during Kafka initialization: " + e.getMessage(), e);
				System.exit(1);
			}
        }

		// Tells the server to return pretty-printed responses by default
		setDefaultPrettyPrint(true);

		// Default to JSON encoding
		setDefaultResponseEncoding(EncodingEnum.JSON);

		logger.info("FhirInputService initialized");
	}
}

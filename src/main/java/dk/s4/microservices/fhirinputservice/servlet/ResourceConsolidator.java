package dk.s4.microservices.fhirinputservice.servlet;

import static org.apache.commons.lang3.StringUtils.isNotBlank;


import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import dk.s4.microservices.microservicecommon.fhir.CarePlanVerifier;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hl7.fhir.instance.model.api.IBaseReference;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Media;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ResourceType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Utility class for consolidating and validating incoming resources
 */
public class ResourceConsolidator {
    private static final Logger logger = LoggerFactory.getLogger(ResourceConsolidator.class);

    private static final String MDC_URN = "urn:iso:std:iso:11073:10101";
    private static final String MDC_SHORT_NAME = "MDC";

    private List<String> requiredCodingSystems = new ArrayList<>();
    private Map<String, String> codingSystemsShortNames = new HashMap<>();

    private ResolverInterface patientResolver;
    private ResolverInterface deviceResolver;
    private ResolverInterface mediaFacade;
    private CarePlanVerifier carePlanVerifier;

    public ResourceConsolidator(PatientResolver patientResolver, DeviceResolver deviceResolver, MediaFacade mediaFacade, CarePlanVerifier carePlanVerifier) {
        this.patientResolver = patientResolver;
        this.deviceResolver = deviceResolver;
        this.mediaFacade = mediaFacade;
        this.carePlanVerifier = carePlanVerifier;
        setupRequiredCodingSystems();
        setupCodingSystemShortnames();
    }

    private void setupRequiredCodingSystems() {
        try {
            JSONParser parser = new JSONParser();
            String requriedCodingSystemsList = System.getenv("FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS");
            if (requriedCodingSystemsList != null && !requriedCodingSystemsList.isEmpty()) {
                JSONArray jsonArray = null;
                jsonArray = (JSONArray)parser.parse(requriedCodingSystemsList);
                for (Object object: jsonArray) {
                    if (object instanceof String)
                        requiredCodingSystems.add((String)object);
                    else {
                        String errMsg = "Error parsing FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS: Expected array of strings";
                        logger.error(errMsg);
                        throw ExceptionUtils.fatalInternalErrorException(errMsg);
                    }
                }
            }
        } catch (ParseException e) {
            logger.error("Error parsing FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS: " + e.getMessage(), e);
            throw ExceptionUtils.fatalInternalErrorException("Error parsing FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS: " + e.getMessage());
        }
    }

    private void setupCodingSystemShortnames() {
        try {
            JSONParser parser = new JSONParser();
            String codingSystemsShortNamesList = System.getenv("FHIRINPUTSERVICE_CODING_SYSTEMS_SHORTNAMES");
            if (codingSystemsShortNamesList != null && !codingSystemsShortNamesList.isEmpty()) {
                JSONArray jsonArray = null;
                jsonArray = (JSONArray)parser.parse(codingSystemsShortNamesList);
                for (Object object: jsonArray) {
                    if (object instanceof JSONObject) {
                        JSONObject jsonObject = (JSONObject) object;
                        codingSystemsShortNames.put((String) jsonObject.get("system"), (String) jsonObject.get("shortName"));
                    }
                    else {
                        String errMsg = "Error parsing FHIRINPUTSERVICE_CODING_SYSTEMS_SHORTNAMES: Expected array of JSON objects";
                        logger.error(errMsg);
                        throw ExceptionUtils.fatalInternalErrorException(errMsg);
                    }
                }
            }
        } catch (ParseException e) {
            logger.error("Error parsing FHIRINPUTSERVICE_CODING_SYSTEMS_SHORTNAMES: " + e.getMessage(), e);
            throw ExceptionUtils.fatalInternalErrorException("Error parsing FHIRINPUTSERVICE_CODING_SYSTEMS_SHORTNAMES: " + e.getMessage());
        }
    }

    private String getCodeSystemShortName(String codeSystem) {
        return codingSystemsShortNames.get(codeSystem);
    }

    /**
     * Validates that FHIR Observation has an code in FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS (given by env.var.) and looks
     * up Patient resources replacing existing reference with a logical reference using the official patient
     * identifier system (given by env.var.). Similarly looks up Device resources replacing existing reference
     * with a logical reference using the official device identifier system (given by env.var.)
     *
     * @param observation
     *            The observation to consolidate
     * @param isContained Is it a contained resource? Some fields are not mandatory on contained resources
     * @param consolidatePatient Should we consolidate the patient or not?
     * @param consolidateDevice Should we consolidate the device or not?
     * @return String Code value in one of the FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS
     */
    public String consolidateObservation(Observation observation, boolean isContained,
                                                 boolean consolidatePatient, boolean consolidateDevice) {
        String code = null;

        if (!isContained){
            if (!carePlanVerifier.isActiveCarePlan(observation.getBasedOnFirstRep().getIdentifier())){
                throw ExceptionUtils.fatalFormatException("The CarePlan references in basedOn is not active");
            }
        }

        //Check that we have a code and that one of the codings is from the required observation coding system
        if (!observation.hasCode()) {
            throw ExceptionUtils.fatalFormatException("No code provided, FHIR Observation must have a code.");
        }
        if (!observation.getCode().hasCoding()) {
            throw ExceptionUtils.fatalFormatException("Code has no coding element, FHIR Observation must have a code with coding element(s).");
        }

        boolean hasRequiredcoding = false;
        for (Coding coding : observation.getCode().getCoding()) {
            if (coding.hasSystem() && coding.hasCode()) {
                if (requiredCodingSystems.contains(coding.getSystem())) {
                    code = getCodeSystemShortName(coding.getSystem()) + coding.getCode();
                    logger.debug("Validated Observation with required coding = {}", code);
                    hasRequiredcoding = true;
                }
            }
            else {
                throw ExceptionUtils.fatalFormatException("Observation has coding element where system or code is missing");
            }
        }
        if (!hasRequiredcoding) {
            throw ExceptionUtils.fatalFormatException("No coding element with one of the required coding systems: " +
                    System.getenv("FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS"));
        }

        if (consolidatePatient) {
            //Consolidate patient reference into the observation
            IBaseResource subject = observation.getSubject().getResource();
            if (subject == null)
                throw ExceptionUtils.fatalFormatException("Observation must have a subject");
            observation.setSubject(patientResolver.getReference(subject));

            if (!observation.hasPerformer() && !isContained)
                throw ExceptionUtils.fatalFormatException("Observation must have a performer");
            List<Reference> consolidatedPerformers = new ArrayList<>();
            for (Reference performerRef : observation.getPerformer()) {
                IBaseResource performer = performerRef.getResource();
                if (performer instanceof Patient)
                    consolidatedPerformers.add(patientResolver.getReference(performer));
                else
                    throw ExceptionUtils.fatalFormatException("Non-patient performers are not (yet) supported on observations");
            }
            observation.getPerformer().clear();
            for (Reference ref : consolidatedPerformers)
                observation.addPerformer(ref);
        }

        if (consolidateDevice) {
            //Consolidate device reference into the observation
            IBaseResource device = observation.getDevice().getResource();
            if (device != null) {
                Reference devRef = deviceResolver.getReference(device);
                ((DeviceResolver)deviceResolver).createConditionally((Device)device, devRef);
                observation.setDevice(devRef);
            }
            else //We allow observations with no device reference, so don't throw formatExcep. if null
                logger.warn("No Device reference found on Observation");

            //Consolidate PHG (Personal Health Gateway) reference into the observation
            Extension phgExtension = observation.getExtensionByUrl(System.getenv("PHG_OBSERVATION_EXTENSION_URL"));
            if (phgExtension != null) {
                IBaseResource phgDevice = ((Reference)phgExtension.getValue()).getResource();
                if (phgDevice != null) {
                    Reference phgDevRef = deviceResolver.getReference(phgDevice);
                    ((DeviceResolver)deviceResolver).createConditionally((Device)phgDevice, phgDevRef);
                    phgExtension.setValue(phgDevRef);
                }
                else //We allow observations with no PHG reference, so don't throw formatExcep. if null
                    logger.warn("No PHG reference found on Observation");
            }
            else //We allow observations with no PHG reference, so don't throw formatExcep. if null
                logger.warn("No PHG reference found on Observation");
        }

        if (observation.hasContained()) {
            for (Resource containedRes: observation.getContained()) {
                if (containedRes instanceof Observation)
                    //Notice: We don't pickup codes from contained resources
                    consolidateObservation((Observation)containedRes, true, consolidatePatient, consolidateDevice);
            }
        }

        return code;
    }

    /**
     * Looks up Patient resources replacing existing reference with a logical reference using the official patient
     * identifier system (given by env.var.)
     *
     * @param questionnaireResponse
     *            The QuestionnaireReponse to consolidate
     */

    private void consolidateQuestionnaireResponse(QuestionnaireResponse questionnaireResponse) {
        //Consolidate patient reference into QuestionnaireResponse
        if (!carePlanVerifier.isActiveCarePlan(questionnaireResponse.getBasedOnFirstRep().getIdentifier())){
            throw ExceptionUtils.fatalFormatException("The CarePlan references in basedOn is not active");
        }

        IBaseResource subject = questionnaireResponse.getSubject().getResource();
        if (subject == null)
            throw ExceptionUtils.fatalFormatException("QuestionnaireResponse has no subject");
        questionnaireResponse.setSubject(patientResolver.getReference(subject));

        IBaseResource author = questionnaireResponse.getAuthor().getResource();
        if (author == null)
            throw ExceptionUtils.fatalFormatException("Author must be specified on QuestionnaireResponse");
        if (author instanceof Patient)
            questionnaireResponse.setAuthor(patientResolver.getReference(author));
        else
            throw ExceptionUtils.fatalFormatException("Non-patient authors are not (yet) supported on QuestionnaireResponse");

        IBaseResource source = questionnaireResponse.getSource().getResource();
        if (source == null)
            throw ExceptionUtils.fatalFormatException("Source must be specified on QuestionnaireResponse");
        questionnaireResponse.setSource(patientResolver.getReference(source));

        //Consolidate PHG (Personal Health Gateway) reference into the questionnaire response
        Extension phgExtension = questionnaireResponse.getExtensionByUrl(System.getenv("PHG_QUESTIONNAIRERESPONSE_EXTENSION_URL"));
        if (phgExtension != null) {
            IBaseResource phgDevice = ((Reference)phgExtension.getValue()).getResource();
            if (phgDevice != null) {
                Reference phgDevRef = deviceResolver.getReference(phgDevice);
                ((DeviceResolver)deviceResolver).createConditionally((Device)phgDevice, phgDevRef);
                phgExtension.setValue(phgDevRef);
            }
            else //We allow observations with no PHG reference, so don't throw formatExcep. if null
                logger.warn("No PHG reference found on QuestionnaireResponse");
        }
        else //We allow observations with no PHG reference, so don't throw formatExcep. if null
            logger.warn("No PHG reference found on QuestionnaireResponse");
    }

    /**
     * Extract first Identifier from Patient and return it as a logical reference
     * @param patient Patient resource
     * @return Logical reference
     * @throws UnprocessableEntityException
     *
     */
    public Reference createReferenceWithIdentifierFrom(Patient patient) {
        if (patient.hasIdentifier()) {
            return new Reference().setIdentifier(patient.getIdentifier().get(0));
        }
        else
            throw ExceptionUtils.fatalFormatException("Patient has no identifiers");
    }

    /**
     * Consolidate fullUrl references into logical references
     *
     * @param media Media Resource
     * @throws UnprocessableEntityException
     */
    private void consolidateMedia(Media media) {
        //Replace fullUrl references with logical references
        IBaseResource subject = media.getSubject().getResource();
        if (subject == null)
            throw ExceptionUtils.fatalFormatException("Media must have a subject");
        if (subject instanceof Patient) {
            media.setSubject(createReferenceWithIdentifierFrom((Patient) subject));
        }
        else
            throw ExceptionUtils.fatalFormatException("Media with non-Patient subject is not supported");

        //We treat missing operator reference leniently
        IBaseResource operator = media.getOperator().getResource();
        if (operator != null) {
            if (operator instanceof Patient)
                media.setOperator(createReferenceWithIdentifierFrom((Patient) operator));
            else
                throw ExceptionUtils.fatalFormatException("Media with non-Patient operator is not supported");
        }
        else
            logger.warn("Media does not have an operator");

        //We treat missing partOf reference leniently
        if (!media.getPartOf().isEmpty()) {
            IBaseResource partOf = media.getPartOf().get(0).getResource();
            if (partOf != null && partOf instanceof Observation) {
                Observation observation = (Observation)partOf;
                if (observation.hasIdentifier()) {
                    media.getPartOf().clear();
                    media.addPartOf().setIdentifier(observation.getIdentifier().get(0));
                }
                else
                    logger.warn("Media is referencing Observation that doesn't have an identifier");
            }
            else
                logger.warn("Media's partOf reference does not resolve / is not an Observation");
        }
        else
            logger.warn("Media does not have a partOf reference");

        //Consolidate device reference into the observation
        IBaseResource device = media.getDevice().getResource();
        if (device != null) {
            Reference devRef = deviceResolver.getReference(device);
            ((DeviceResolver)deviceResolver).createConditionally((Device)device, devRef);
            media.setDevice(devRef);
        }
        else //We allow observations with no device reference, so don't throw formatExcep. if null
            logger.warn("No Device reference found on Observation");
    }

    /**
     * Validates and consolidates contents of Bundle and returns codes of observation(s)
     * in one of the required coding systems (given by env.var. FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS).
     *
     * Validates that a Patient entry is present.
     * Does basic validation on each of the entries.
     *
     * Consolidates Patient references. I.e. looks up patient on external service and fetches identifier in
     * OFFICIAL_PATIENT_IDENTIFIER_SYSTEM (env.var.) system. Then replaces all patient references in
     * QuestionnaireResponse and Observation resource with a FHIR logical reference to this identifier.
     * Finally removes the Patient resource from the Bundle.
     *
     * Consolidates Device references. From Bundle local Device entry, takes the identifier in
     * OFFICIAL_DEVICE_IDENTIFIER_SYSTEM (env.var.) system, then replaces all device references in
     * QuestionnaireResponse and Observation resource with a FHIR logical reference to this identifier.
     * Finally removes the Device resource from the Bundle.
     *
     * @param bundle
     *            The bundle to consolidate
     * @return List of the required codes of the Observation resources contained in the
     * Bundle (not including Observation resources contained in other Observation resources)
     *
     * @see <a href="http://hl7.org/fhir/2018Sep/references.html#logical">FHIR logical reference</a>
     *
     */
    public List<String> consolidateBundleAndGetCoding(Bundle bundle) throws ResourceNotFoundException {
        if (!bundle.hasEntry()) {
            throw ExceptionUtils.fatalFormatException("Bundle has no entries");
        }

        //HAPI FHIR ParserState.stitchBundleCrossReferences() really ought have done this but it has issues with references
        //and fullUrl's that are neither URLs nor URNs but just a generic URI like our Device references.
        stitchBundleCrossReferences(bundle);

        List<String> codes = new ArrayList<>();
        Bundle.BundleEntryComponent patientEntry = null;
        List<Bundle.BundleEntryComponent> deviceEntries = new ArrayList<>();
        List<Bundle.BundleEntryComponent> mediaEntries = new ArrayList<>();

        for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
            if (entry.isEmpty())
                continue;
            if (entry.getResource() == null)
                continue;

            if (entry.getResource().getResourceType().equals(ResourceType.Observation)) {
                codes.add(consolidateObservation((Observation) entry.getResource(), false, true, true));
             }
            else if (entry.getResource().getResourceType().equals(ResourceType.QuestionnaireResponse)) {
                consolidateQuestionnaireResponse((QuestionnaireResponse) entry.getResource());
            }
            else if (entry.getResource().getResourceType().equals(ResourceType.Patient)) {
                patientEntry = entry;
            }
            else if (entry.getResource().getResourceType().equals(ResourceType.Device)) {
                deviceEntries.add(entry);
                Device device = (Device)entry.getResource();
                if (device.hasType() && device.getType().hasCoding()) {
                    boolean devHasReqCoding = false;
                    for (Coding coding: device.getType().getCoding()) {
                        if (requiredCodingSystems.contains(coding.getSystem()) && coding.hasCode()) {
                            devHasReqCoding = true;
                        }
                    }
                    if (!devHasReqCoding)
                        throw ExceptionUtils.fatalFormatException("Device is missing required coding ("
                                + System.getenv("FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS") + ") on type");
                }
                else
                    throw ExceptionUtils.fatalFormatException("Type unspecified or malformed on Device");

            }

            if (System.getenv("MEDIA_CREATE_SEPARATELY").equalsIgnoreCase("true")) {
                if (entry.getResource().getResourceType().equals(ResourceType.Media)) {
                    mediaEntries.add(entry);
                    Media media = (Media) entry.getResource();
                    consolidateMedia(media);

                    //Create Media resource on Media service
                    Reference mediaRef = mediaFacade.getReference(media);
                    ((MediaFacade)mediaFacade).create(media, mediaRef);
                }
            }
        }

        //Patient resource must be present
        if (patientEntry == null) {
            throw ExceptionUtils.fatalFormatException("No patient found in Bundle");
        }

        //Remove patient from bundle after consolidation, since we expand the .subject references to the patient into
        //logical references to Patient resource on external service.
        List<Bundle.BundleEntryComponent> bundleEntry = bundle.getEntry();
        bundleEntry.remove(patientEntry);

        //Remove device entries from bundle after consolidation, since we expand the .device references to the device into
        //logical references to Device resource on external service.
        for (Bundle.BundleEntryComponent devEntry : deviceEntries)
            bundleEntry.remove(devEntry);

        if (System.getenv("MEDIA_CREATE_SEPARATELY").equalsIgnoreCase("true")) {
            //Remove media entries from bundle after consolidation, when we expand the media references into
            //logical references to Media resource on external service.
            for (Bundle.BundleEntryComponent devEntry : mediaEntries)
                bundleEntry.remove(devEntry);
        }

        logger.trace("Bundle after consolidation: \n" +
                FhirInputService.getMyFhirContext().newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));

        return codes;
    }

    //HAPI FHIR ParserState.stitchBundleCrossReferences() has issues with references
    //and fullUrl's that are neither URLs nor URNs but just a generic URI like our Device references. This method
    //patches these issues.
    private void stitchBundleCrossReferences(Bundle bundle) {
        Map<String, Resource> idToResource = new HashMap<String, Resource>();

        for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
            String fullUrl = entry.getFullUrl();
            if (fullUrl != null && isNotBlank(fullUrl)) {
                Resource resource = entry.getResource();
                if (resource != null) {
                    idToResource.put(resource.getResourceType() + "/" + fullUrl, resource);
                }
            }
        }

        List<IBaseResource> resources = FhirInputService.getMyFhirContext().newTerser().getAllPopulatedChildElementsOfType(bundle, IBaseResource.class);
        for (IBaseResource next : resources) {
            List<IBaseReference> refs = FhirInputService.getMyFhirContext().newTerser().getAllPopulatedChildElementsOfType(next, IBaseReference.class);
            for (IBaseReference nextRef : refs) {
                if (nextRef.isEmpty() == false && nextRef.getReferenceElement() != null) {
                    IIdType unqualifiedVersionless = nextRef.getReferenceElement().toUnqualifiedVersionless();
                    logger.debug(unqualifiedVersionless.getValueAsString());
                    logger.debug(unqualifiedVersionless.getValue());
                    logger.debug(unqualifiedVersionless.getResourceType());
                    logger.debug(unqualifiedVersionless.getIdPart());
                    IBaseResource target = idToResource.get(unqualifiedVersionless.getValueAsString());
                    if (target != null) {
                        nextRef.setResource(target);
                    }
                }
            }
        }
    }
}

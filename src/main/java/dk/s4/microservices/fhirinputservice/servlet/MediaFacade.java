package dk.s4.microservices.fhirinputservice.servlet;

import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.security.ThreadLocalContext;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Media;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Contains functionality in relation to referencing and creating Media resources
 */
public class MediaFacade implements ResolverInterface {
    private static final Logger logger = LoggerFactory.getLogger(MediaFacade.class);

    /**
     * From a Bundle local Media resource get a logical reference based on the official identifier system.
     *
     * @param bundleLocalResource Bundle local Media resource
     * @return Logical reference to Media in the form of a HAPIFHIR Reference object.
     */
    public Reference getReference(IBaseResource bundleLocalResource) {
        logger.debug("getReference");
        Identifier localIdentifier = new Identifier();
        for (Identifier identifier: ((Media) bundleLocalResource).getIdentifier()) {
            if (identifier.hasSystem()
                    && identifier.hasValue()
                    && identifier.getSystem().equals(System.getenv("OFFICIAL_MEDIA_IDENTIFIER_SYSTEM"))) {
                localIdentifier = identifier;
                logger.debug("Media identifier: system = {} value = {}", localIdentifier.getSystem(),
                        localIdentifier.getValue());
            }
        }
        if (!localIdentifier.hasSystem())
            throw ExceptionUtils.fatalFormatException("Media doesn not have required identifier with system = "
                    + System.getenv("OFFICIAL_MEDIA_IDENTIFIER_SYSTEM"));

        return new Reference().setIdentifier(localIdentifier).setType("Media");
    }

    /**
     * Emit event over messaging system to create Media resource.
     *
     * @param media FHIR Media resource
     * @param reference logical reference to the media
     */
    public void create(Media media, Reference reference) {
        Message message = new Message().
                setSender(System.getenv("SERVICE_NAME")).
                setBodyCategory(Message.BodyCategory.FHIR).
                setBodyType("Media").
                setContentVersion(System.getenv("FHIR_VERSION")).
                setCorrelationId(MDC.get(System.getenv("CORRELATION_ID"))).
                setTransactionId(MDC.get("transactionId")).
                setSession(ThreadLocalContext.get("SESSION")).
                setSecurity(ThreadLocalContext.get("SECURITY_TOKEN")).
                setBody(FhirInputService.getMyFhirContext().newJsonParser().encodeResourceToString(media));
        Topic topic = new Topic().
                setOperation(Topic.Operation.Create).
                setDataCategory(Topic.Category.FHIR).
                setDataType(media.fhirType());
        FhirInputService.eventProducer.sendMessage(topic, message);
    }
}

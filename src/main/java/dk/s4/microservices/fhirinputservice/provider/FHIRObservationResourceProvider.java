package dk.s4.microservices.fhirinputservice.provider;

import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR Observation
 * resource, which supports the Create operation.
 */
public class FHIRObservationResourceProvider implements IResourceProvider {

	private static final Logger logger = LoggerFactory.getLogger(FHIRObservationResourceProvider.class);

	private ResourceHandler resourceHandler;

	public FHIRObservationResourceProvider(ResourceHandler resourceHandler) {
		this.resourceHandler = resourceHandler;
	}

	/**
	 * Returns the type of resource this provider supplies.
	 */
	@Override
	public Class<Bundle> getResourceType() {
		return Bundle.class;
	}

	/**
	 * Creates a Bundle resource.
	 *
	 * The FHIR Bundle to create is put on an async queue in order to be processed later on.
	 *
	 * @param observation The Observation to create
	 * @param theRequest Incoming http request
	 * @param theResponse Outgoing http response
	 * @return MethodOutcome
	 */
	@Create()
	public MethodOutcome create(@ResourceParam Observation observation,
								HttpServletRequest theRequest,
								HttpServletResponse theResponse) {
		logger.debug("create observation");
		return resourceHandler.handleCreateResource(observation, theRequest, theResponse);
	}

	public void setResourceHandler(ResourceHandler resourceHandler) {
		this.resourceHandler = resourceHandler;
	}
}

package dk.s4.microservices.fhirinputservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.fhirinputservice.servlet.FhirInputService;
import dk.s4.microservices.fhirinputservice.servlet.ResourceConsolidator;
import dk.s4.microservices.messaging.EventProducer;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.security.ThreadLocalContext;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.errors.TimeoutException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;

import static net.logstash.logback.argument.StructuredArguments.v;

/** Singleton class containing shared functionality for handling creation of resources and for handling user
 * verification in relation to CarePlans
 *
 */
public class ResourceHandler {
    private static final Logger logger = LoggerFactory.getLogger(ResourceHandler.class);
    private static final String OO_SEVERITY_INFO = "information";

    private final ResourceConsolidator resourceConsolidator;

    public ResourceHandler(ResourceConsolidator resourceConsolidator) {
        this.resourceConsolidator = resourceConsolidator;
    }

    /**
     * Takes incoming resource, validates and consolidates it using {@link ResourceConsolidator} and forwards
     * consolidated resource to messaging system
     *
     * @param resource FHIR Bundle or Observation
     * @param theRequest Http request
     * @param theResponse Http response
     * @return FHIR MethodOutcome
     */
    public MethodOutcome handleCreateResource(Resource resource,
                                              HttpServletRequest theRequest,
                                              HttpServletResponse theResponse) {

        MethodOutcome retVal = new MethodOutcome();
        OperationOutcome operationOutcome = new OperationOutcome();
        OperationOutcome.OperationOutcomeIssueComponent issue = operationOutcome.addIssue();
        retVal.setOperationOutcome(operationOutcome);

        try {
            List<String> codes = new ArrayList<>();
            if (resource instanceof Bundle)
                codes =resourceConsolidator.consolidateBundleAndGetCoding((Bundle) resource);
            else if (resource instanceof Observation)
                codes.add(resourceConsolidator.consolidateObservation((Observation)resource,
                        false, false, false));

            sendResourceToMsgQueue(resource, resource.fhirType(), codes, theRequest.getHeader("SESSION"),
                    FhirInputService.eventProducer, FhirInputService.getMyFhirContext());

            issue.getSeverityElement().setValueAsString(OO_SEVERITY_INFO);
            issue.setDiagnostics("Successfully received resource");
        } catch (InterruptedException | AuthenticationException | IllegalStateException | KafkaException e) {
            throw ExceptionUtils.fatalInternalErrorException("Fatal error receiving resource. Failed when publishing ", e);
        } catch (ResourceNotFoundException e) {
            throw ExceptionUtils.fatalInternalErrorException("Lookup of resource referenced from resource inside " +
                    "bundle threw ResourceNotFoundException", e);
        }

        return retVal;
    }

    static void sendResourceToMsgQueue(IBaseResource resource, String resourceType, List<String> codes,
                                       String session, EventProducer eventProducer, FhirContext context)
            throws CancellationException, InterruptedException, AuthenticationException, SerializationException,
            TimeoutException, KafkaException {

        Topic topic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType(resourceType);

        for (String code: codes)
            topic.addDataCode(code);

        logger.debug("Sending resource to topic: {}", v("kafka_topic", topic));

        Message message = new Message()
                .setSender(System.getenv("SERVICE_NAME"))
                .setBodyCategory(Message.BodyCategory.FHIR)
                .setBodyType(resourceType)
                .setContentVersion(System.getenv("FHIR_VERSION"))
                .setCorrelationId(MDC.get(System.getenv("CORRELATION_ID")))
                .setTransactionId(ThreadLocalContext.get(System.getenv("TRANSACTION_ID")))
                .setSecurity(ThreadLocalContext.get("SECURITY_TOKEN"))
                .setSession(session)
                .setBody(context.newJsonParser().encodeResourceToString(resource));

        eventProducer.sendMessage(topic, message);
    }
}

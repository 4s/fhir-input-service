# FHIR Input Service
The purpose of this service is to receive data from the patient environment in a remote monitoring scenario. 
Specifically the FHIR Input Service will sit at the server side and can be used when a remote client (e.g. an app) on a 
mobile device wants to upload measurements and/or questionnaire responses in HL7 FHIR format.

The fhir-input-service is a microservice intented for use in a microservice based system as the one described at 
[GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md). The service is a 
_input service_ as described in [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md) 
and provides a RESTful FHIR API which it is able to receive FHIR Bundles containing FHIR Observation, 
QuestionnaireResponse, DeviceComponent and Patient resources. See more about the use of these FHIR resources and how
 they relate to other resources in the system on the pages about the
 [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 

The implementation of the service takes departure in Continua Design Guidelines as specified in _H.812.5 FHIR Observation Upload_.
 
When receiving data the service will do basic validation of the data received, resolve given 
patient identifiers into a full resource references and produce the resulting bundle as a message on the 
configured messaging system which in turn will result in the message being forwarded to downstream microservices
consuming on the output topics of the fhir-input-service. 
 
The FHIR Input Service supports HL7 FHIR R4.

## Design

### Upload of measurements
The implementation of the service follows Continua Guidelines for upload of measurements via HL7 FHIR, _H.812.5 FHIR 
Observation Upload_ (H.812.5). The service implements a FHIR Observation Reporting Server:

_FHIR Observation Reporting Server - A H&FS  that requires the complete context of a measurement to be contained in 
the received application data packet. In FHIR this means that the received message contains a complete bundle. A 
complete bundle is one in which all resources associated with the measurement are pre-sent._ (H.812.5)

This means that the input service requires all Personal Health Gateways to comply with the guidelines for a FHIR 
Observation Reporting Client:

_FHIR Observation Reporting Client– A PHG   that bundles all resources associated with a given sensor measurement 
into a single application data packet._ (H.812.5)

Specifically this means that this service expects PHGs to upload a FHIR transaction Bundle containing information 
about the patient (FHIR Patient), the measurement(s) (FHIR Observation(s)) and measurement Devices (FHIR Devices).

Patient information in such bundles is only expected to contain a simple identifier. This identifier should 
uniquely identify the Patient in an active remote monitoring context. Typically this identifier is obtained by PHGs 
as an attribute on a login token.

### Upload of questionnaire responses
Continue have not yet profiled uploading of questionnaire responses via FHIR. However, this service implements the 
uploading of questionnaire responses in an analogous way to the FHIR Observation Reporting Server, mentioned above. 
That is, the FHIR Input Service expects expects PHGs to upload a FHIR transaction Bundle containing information 
about the patient (FHIR Patient), the questionnaire response (FHIR QuestionnaireResonse) and measurement Devices (FHIR 
Devices).

### Upload of measurements and questionnaire responses
If wanting to upload measurements along with questionnaire responses - if for instance the measurement was taken as 
part of answering the questionnaire - then a FHIR transaction Bundle containing all of the above mentioned parts - 
patient, measurements, questionnaire response and devices - is expected.

# FHIR Input Service:

| Summary    |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Service name (computer friendly) | fhir-input-service                                                                                                        |
| Maturity level                   | Concept [ ] Prototype [ ] Demonstration [x] Product [ ]                                                                                                         |
| Used externally                  | Yes, used by mobile app for patients                                                                   |
| Database                         | no                                                                                                                  |
| RESTful dependencies             | patient-care-service   |
| Messaging dependencies           | Kafka                                                                                                               |
| Other dependencies               | In DIAS configuration: User context service (user context info from session ID)                                                            |
| License                          | Apache 2.0                                                                                                          |

The fhir-input-service is a microservice intented for use in a microservice based system as the one described at 
[GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md). The service is a 
_input service_ as described in [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md) 
and provides a RESTful FHIR API which it is able to receive FHIR Bundles containing FHIR Observation, 
QuestionnaireResponse, DeviceComponent and Patient resources. See more about the use of these FHIR resources and how
 they relate to other resources in the system on the pages about the
 [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 

## Further documentation

  - [GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md)
  - Design, FHIR API and messaging API documentation: See [docs](docs//) folder.
  - Use of these FHIR resources and how they relate to other resources in the system on the pages about the [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 
  - Public classes and methods are documented with JavaDoc 
  - More on [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/).

## Prerequisites
To build the project you need:

  * OpenJDK 11+
  * Maven 3.1.1+
  * Docker 20+

The project depends on git submodule (ms-scripts). To initialize this you need to execute:

```
git submodule update --init
git submodule foreach git pull origin master
``` 

If you are running things using docker-compose (see below) you also need to have a docker network by the name of 
`opentele3net` setup:

`docker network create opentele3net` 

## Building the service
The simplest way to build the service is to use the `buildall.sh` script:

`/scripts/buildall.sh`

This will build the service as a WAR file and add this to a Jetty based docker image.

## Running the service
You can run the service just using ``docker run ...`` or you can run the service using the provided docker-compose files. 
You may of course also run the service in container-orchestration environments like Kubernetes or Docker Swarm.

### Running the service with docker-compose
With docker-compose you can run the service like this:

`docker-compose up`

This will start the service with a Jetty running on port 8080. To run the service with a different port mapping use:

`docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml up`

## Logging
The service uses Logback for logging. At runtime Logback log levels can be configured via environment variables (see
below). 

_Notice:_ The service may output person sensitive information at log level `TRACE`. If log level is set to `DEBUG` or 
above no person sensitive information should be output from the service.

If you are running via docker-compose and you want use the fluentd logging driver (if you for instance have setup 
fluentd, elasticsearch and Kibana) instead of the standard json-file you may start the service like this:

```
docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up
```

## Monitoring
On http the service exposes a health endpoint on the path `/info`. This will return `200 ok` if the service is basically
in a healthy state, meaning that the web service is up and running inside Jetty.

On http the service exposes Prometheus metrics on the path `/metrics`. 

If you set environment variable `DIAS_AUDIT_ENABLED` to `true` and point `DIAS_AUDIT_URL` to a [DIAS Auditlog compatible
service](https://telemed-test.rm.dk/leverandorportal/leverance/_logning/) the employee-bff will log audit events to such a service. 

## Environment variables

The files `service.env` and `deploy.env` define all environment variables used by this microservice. `deploy.env` defines
service URLs and log levels that depend on your particular deployment.

 | Environment variable          | Required | Description                                                                                                                     |
 |-------------------------------|----------|----------------------------------------------------------------------
 | SERVICE_NAME                  | y | Computer friendly name of this service.
 | FHIR_VERSION                  | y | Version of HL7 FHIR supported by this service
 | SERVER_BASE_URL               | n | The full address of the service as seen by clients. If the service for example is behind an API gateway, the service base URL must be set.
 | *Log levels:*                 ||
 | LOG_LEVEL                     | y | Set the log level for 3rd party libraries
 | LOG_LEVEL_DK_S4               | y | Set the log level for this service and the 4S libraries used by this service
 | *Authentication*              | |
 | ENABLE_AUTHENTICATION         | n | Enable getting user context information (from http session header or otherwise)
 | ENABLE_DIAS_AUTHENTICATION    | n | Enable DIAS specific authentication functionality
 | USER_CONTEXT_SERVICE_URL      | n | URL of user context side care service. Required if ENABLE_DIAS_AUTHENTICATION=true
 | *Keycloak related:*           | |
 | ENABLE_AUTH                   | y | Enable Keycloak authentication. *Warning:* Integration w. Keycloak needs (re-)testing!
 | KEYCLOAK_REALM                | n | Keycloak realm - see https://www.keycloak.org/docs/3.0/server_admin/topics/overview/concepts.html.
 | KEYCLOAK_CLIENT_NAME          | n | Keycloak client name used by this service - see 'clients' at https://www.keycloak.org/docs/3.0/server_admin/topics/overview/concepts.html.
 | AUTH_SERVER_URL               | n | URL of the Keycloak authentication server
 | *Messaging related:*          | |
 | ENABLE_KAFKA                  | y | If `true Kafka communication is enabled for this service
 | KAFKA_BOOTSTRAP_SERVER        | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
 | KAFKA_GROUP_ID                | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
 | KAFKA_ACKS                    | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
 | KAFKA_RETRIES                 | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
 | KAFKA_KEY_SERIALIZER          | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
 | KAFKA_VALUE_SERIALIZER        | n | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
 | *Coding systems:*             | |
 | OFFICIAL_PATIENT_IDENTIFIER_SYSTEM | y | The official identifier system profiled for use in your FHIR patient resources. In Denmark this must be the CPR system (urn:oid:1.2.208.176.1.2)
 | OFFICIAL_DEVICE_IDENTIFIER_SYSTEM | y | The official identifier system profiled for use in your FHIR device resources.
 | FHIRINPUTSERVICE_REQUIRED_CODING_SYSTEMS | y | List of coding system where one of the systems in the list is required to be present on FHIR Observation resources provided to the service
 | FHIRINPUTSERVICE_CODING_SYSTEMS_SHORTNAMES | y | List of mappings between full coding system name and short coding system name. The latter is used for constructing outgoing topics.
 | *Extension urls:*             | |
 | PHG_OBSERVATION_EXTENSION_URL | y | On FHIR Observation resources, the URL of the extension that is used to identify the Personal Health Gateway device.
 | PHG_QUESTIONNAIRERESPONSE_EXTENSION_URL | y | On FHIR Questionnaire resources, the URL of the extension that is used to identify the Personal Health Gateway device.
 | *Service URLs*                ||
 | PATIENTCARE_SERVICE_URL       | y | The full address of the service where patients can be looked up
 | *Data handling*                ||
 | MEDIA_CREATE_SEPARATELY       | y | Extract Media ressources from Bundles and create them separately
 
 More on [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/).

## Test
To do a basic test of the service you may run

```
mvn clean verify
```

### Smoketest without authentication
Set `ENABLE_AUTHENTICATION=false` and `ENABLE_DIAS_AUTHENTICATION=false` and run the service like this:

`docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml up`

``
curl http://localhost:8087/info/
``

The expected output of the curl command is a status code 200. The output should contain:

```
{"status":"all good"}
```

Smoketest the FHIR server part of the service by running:

`curl http://localhost:8087/baseR4/metadata`

This should outout a FHIR CapabilityStatement beginning starting with lines like the following:

``
{
  "resourceType": "CapabilityStatement",
  ...
``

### Smoketest with authentication
To smoketest the service with [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/) 
authentication turned on run Set `ENABLE_AUTHENTICATION=true` and `ENABLE_DIAS_AUTHENTICATION=true` and set 
`USER_CONTEXT_SERVICE_URL` to point to a sidecar service with a [getsessiondata](https://telemed-test.rm.dk/leverandorportal/services/sidevogne/getsessiondata/)
endpoint. After his you may test the service as described above, just adding a valid session id to the header of your
calls:

`curl -H "SESSION: my-valid-session-id" http://localhost:8087/baseR4/metadata`

